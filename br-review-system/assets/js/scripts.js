jQuery(document).ready(function($) {
	
	/**
	 * DataTables: Review Archive Table 
	 */
    $('#brrs-review-archives').DataTable(
    	{
    		pageLength: 100,
    		stateSave: true
    	}
    );
    
    $('#brrs-review-publisheroptin').on( "click", function() {
		$( "#brrs-acceptance-option-query-letter" ).toggle();
    });
    
});