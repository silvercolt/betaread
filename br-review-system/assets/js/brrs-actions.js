jQuery(document).ready(function($) {
	
    // set default variables
    var selected_action = null;	
    var selected_value = null;
    var action_comment = null;
    var selected_accreditation = null;
    var acceptance_publisher_optin = null;
    var review_action_assign_dialog = null;
    var review_action_complete_dialog = null;
    var review_action_close__dialog = null;
    var review_action_pull_dialog = null;
    var review_action_accept_dialog = null;
    var review_id = $('#brrs-single-review-actions').data('review');
    var user_id = $('#brrs-single-review-actions').data('user');
    
	/* ------------------------------------------------
	 * Perform the selected action of a selected review
	 * ------------------------------------------------
	 */	
	function performAction( event ) {	
		
		// Could be null as the action is just a status change specified by the action itself
		if(selected_action === 'assign') {
			selected_value = $('#brrs-review-reviewer').val();
		}
		if(selected_action === 'complete' ) {
			selected_value = $('#brrs-review-accreditation').val();
		}
		if(selected_action === 'pull' || selected_action === 'complete' || selected_action === 'close' ) {
			action_comment = $('#brrs-review-comment').val();
		}
		if(selected_action === 'accept' ) {
			acceptance_publisher_optin = $('#brrs-review-publisheroptin').val();
			acceptance_query_letter = $('#brrs-review-queryletter').val();
		}
			
		$.ajax({
	         type : "post",
	         dataType : "json",
	         url : ajax.url,
	         data : {
	         	action: "review_action", 
	         	review_id : review_id, 
	         	action_value : selected_value, 
	         	review_action : selected_action,
	         	comment : action_comment,
	         	current_user_id : user_id,
	         	publisher_optin : acceptance_publisher_optin
	         },
	         success: function(response) {
	         	
	         	$( '#brrs-review-action-' + selected_action + ' > .fa-spinner' ).remove();
	         	$( '.ui-dialog-buttonset > button:nth-child(1) > .fa-spinner' ).remove();
					
	         	console.info("BRRS Review Action", response);
	            review_action_assign_dialog.dialog( "close" );	
	            review_action_complete_dialog.dialog( "close" );
	            review_action_close_dialog.dialog( "close" );
	            review_action_accept_dialog.dialog( "close" );	         	
	         	
	            if(response.code == 1) {
	            	
					$( "#brrs-review-validate-message" ).html("<div class='brrs-validation brrs-validation-success'><i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" + response.message + "</div>");											
					setTimeout(location.reload.bind(location), 5000);
					
	            }
	            else {
	            	
	               $( "#brrs-review-validate-message" ).html( "<div class='brrs-validation brrs-validation-fail'><i class='fa fa-exclamation' aria-hidden='true''></i> &nbsp;&nbsp;" + response.message + "</div>" );
	            
	            }
	            
	         }
		         
	     });
		
	}
	
	/**
	 *--------------------------------
	 * Review Action Dialogs
	 * ------------------------------- 
	 */
	
	/* --- Assign Reviewer Dialog --- */
	review_action_assign_dialog = $( "#brrs-dialog-review-action-assign" ).dialog({
      autoOpen: false,
      height: 250,
      width: 400,
      modal: true,
      buttons: {
        "Assign Reviewer": performAction,
        Cancel: function() {
          review_action_assign_dialog.dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    
    /* --- Complete Review Dialog --- */
	review_action_complete_dialog = $( "#brrs-dialog-review-action-complete" ).dialog({
      autoOpen: false,
      height: 700,
      width: 950,
      modal: true,
      buttons: {
        "Complete Review": performAction,
        Cancel: function() {
          review_action_complete_dialog.dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    
    /* --- Close Review Dialog --- */
	review_action_close_dialog = $( "#brrs-dialog-review-action-close" ).dialog({
      autoOpen: false,
      height: 450,
      width: 400,
      modal: true,
      buttons: {
        "Close Review": performAction,
        Cancel: function() {
          review_action_close_dialog.dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    
    /* --- Accept Review Dialog --- */
	review_action_accept_dialog = $( "#brrs-dialog-review-action-accept" ).dialog({
      autoOpen: false,
      height: 600,
      width: 600,
      modal: true,
      buttons: {
        "Accept Review": performAction,
        Cancel: function() {
          review_action_accept_dialog.dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    
    /** 
     * ------------------------------------------------
	 * Open Action Dialog: ASSIGN
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-assign" ).button().on( "click", function() {
		selected_action = 'assign';
		review_action_assign_dialog.dialog( "open" );
    });
    
    /** 
     * ------------------------------------------------
	 * Open Action Dialog: COMPLETE
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-complete" ).button().on( "click", function() {
		selected_action = 'complete';
		review_action_complete_dialog.dialog( "open" );
    });
    
    /** 
     * ------------------------------------------------
	 * Open Action Dialog: ACCEPT
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-accept" ).button().on( "click", function() {
		selected_action = 'accept';
		review_action_accept_dialog.dialog( "open" );
    });
    
    /** 
     * ------------------------------------------------
	 * Open Action Dialog: CLOSE
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-close" ).button().on( "click", function() {
		selected_action = 'close';
		review_action_close_dialog.dialog( "open" );
    });
    
    /** 
     * ------------------------------------------------
	 * Perform Action: PULL
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-pull" ).button().on( "click", function() {
		selected_action = 'pull';
		$( '#brrs-review-action-' + selected_action ).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
		performAction( event );
    });
    
    /** 
     * ------------------------------------------------
	 * Perform Action: START
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-start" ).button().on( "click", function() {
		selected_action = 'start';
		$( '#brrs-review-action-' + selected_action ).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
		performAction( event );
    });
    
    /** 
     * ------------------------------------------------
	 * Perform Action: APPROVE
	 * ------------------------------------------------
	 */
	$( "#brrs-review-action-approve" ).button().on( "click", function() {
		selected_action = 'approve';
		$( '#brrs-review-action-' + selected_action ).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
		performAction( event );
    });

});