<?php

/**
 * ----------------------------------------------
 * Add a Status column to the Reviews Admin Table
 * Remove columns not required
 * ----------------------------------------------
 */
 add_filter( 'manage_edit-review_columns', 'add_status_admin_column' );
 function add_status_admin_column( $columns ) {
 	
	//add column
	$columns['status'] = __( 'Status', 'BRRS' );
	$columns['accreditation'] = __( 'Accreditation', 'BRRS' );
	unset( $columns['wpseo-links'] );
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-score-readability'] );
	
	return $columns;
	
 }
 
 add_action( 'manage_review_posts_custom_column', 'add_status_admin_column_data' );
 function add_status_admin_column_data( $column ) {
 	
	global $post;
	$review_id = $post->ID;
	
	switch( $column ) {
	
		case 'status' :
			
			$review_status = get_post_meta( $review_id, 'brrs_status', true );
			
			if( empty( $review_status ) || $review_status == 'notset' ) {
				$review_status = "Not Set";
			}				
			echo '<div class="brrs-review-status brrs-' . strtolower( str_replace( " ", "", $review_status ) ) . '"><a href="/wp-admin/post.php?post=' . $review_id . '&action=edit" title="Edit Review">' . $review_status . '</a></div>';
			break;
			
		case 'accreditation' :
			
			$review_accreditation = get_post_meta( $review_id, 'brrs_accreditation', true );
			
			if( empty( $review_accreditation ) || $review_accreditation == 'notset' ) {
				$review_accreditation = "None";
			}				
			echo '<div class="brrs-review-accreditation brrs-' . strtolower( str_replace( " ", "", $review_accreditation ) ) . '">' . $review_accreditation . '</div>';
			break;
			
	}
	
 }