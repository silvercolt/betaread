<?php

/**
 * ========================================
 * Download a review's accreditation package
 * ========================================
 */
add_action( 'admin_post_download_package', 'cb_download_package' );
function cb_download_package() {
	
	BRRS_REVIEW::get_accreditation_package($_POST['review_id']);
	
}
