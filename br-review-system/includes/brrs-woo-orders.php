<?php

/**
 * ----------------------------------------------
 * Create a new Review upon an Order being created
 * ----------------------------------------------
 */
add_action( 'woocommerce_order_status_completed', 'brrs_create_review_on_order' );
function brrs_create_review_on_order( $order_id ) {
	
	global $wpdb;
	
	// Get Order by ID
	$order = wc_get_order( $order_id );
	
	// Create review for each review package ordered
	$review_ids = BRRS_REVIEW::create_review( $order_id, $order );
	
	if( empty( $review_ids ) ) {
		write_log('BRRS: Failed to create Review on Order ('.$order_id.')' );
	}
	else {
		// Update order items with review ID's
		foreach( $review_ids as $order_item_id => $review_id ) {
			wc_add_order_item_meta( $order_item_id, 'brrs_review_id', $review_id); 
			// Add Order Note that a review was successfully created
			$product_id = wc_get_order_item_meta( $order_item_id, '_product_id', true );
			$product_name = get_the_title( $product_id );
			$order->add_order_note( 'Created new Review with ID: ' . $review_id . ' for the purchase of '. $product_name );
			// Delete Gravity Form's Entry of manuscript details
			$result = $wpdb->get_results("SELECT entry_id FROM 696_gf_entry_meta WHERE meta_key = 'woocommerce_order_number' AND meta_value = $order_id" );
			$delete_result = GFAPI::delete_entry( $result[0]->entry_id );
			write_log("Delete Review Entry Result: " . print_r($delete_result, true));
		}
		// Add customer to the Author Role
		$user = new WP_User( $order->get_user_id() );
		$user->add_role( 'author' );
	}	 
	
}

/**
 * ----------------------------------------------
 * Add a Review column to the Orders Admin Table
 * ----------------------------------------------
 */
 add_filter( 'manage_edit-shop_order_columns', 'add_review_admin_column' );
 function add_review_admin_column( $columns ) {
 	
	//add column
	$columns['review'] = __( 'Review', 'BRRS' );
	
	return $columns;
	
 }
 
 add_action( 'manage_shop_order_posts_custom_column', 'add_review_admin_column_data' );
 function add_review_admin_column_data( $column ) {
 	
	global $post;
	$order_id = $post->ID;
	
	switch( $column ) {
	
		case 'review' :
			
			$order = wc_get_order( $order_id );
			$order_items = $order->get_items();
			foreach( $order_items as $order_item_id => $order_item ) {
				$review_id = wc_get_order_item_meta( $order_item_id, 'brrs_review_id', true ); 
				$review_status = get_post_meta( $review_id, 'brrs_status', true );
				$review_assignee_id = get_post_meta( $review_id, 'brrs_reviewer_id', true );
				$review_assignee = get_userdata( $review_assignee_id );
				if( !empty($review_assignee) ) {
					$review_assignee_name = $review_assignee->display_name;
				}
				else {
					$review_assignee_name = "Unassigned";
				}
				if( empty( $review_status ) || $review_status == 'notset' ) {
					$review_status = "Not Set";
				}				
				echo '<div class="shop-order-review-status brrs-' . strtolower( str_replace( " ", "", $review_status ) ) . '"><a href="/wp-admin/post.php?post=' . $review_id . '&action=edit" title="Edit Review">' . $review_status . '</a></div>';
				echo '<small class="shop-order-review-meta">Reviewer: ' . $review_assignee_name . '</small>';
			}
		
	}
	
 }

/**
 * ----------------------------------------------
 * Auto Complete all WooCommerce orders.
 * ----------------------------------------------
 */
//add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

/**
 * ----------------------------------------------
 * Add Reviews Metabox to Admin Shop Order page
 * ----------------------------------------------
 */
 add_action( 'add_meta_boxes', 'brrs_review_details_meta_box' );
 if ( ! function_exists( 'mv_add_meta_boxes' ) ) {
	function brrs_review_details_meta_box() {
		
		// Status Details on Order Admin Page
		add_meta_box(
			'order_review_details', // $id
	        __('Review Details','BRRS'), // $title 
	        'brrs_meta_box_order_show_review_details', // $callback
	        'shop_order', // $page
	        'side', // $context
	        'core'// $priority
		);
		
	}
}
 
 /**
 * -----------------------------------------------------------
 * Cart
 * 
 * Filter out fields from the order details
 * -----------------------------------------------------------
 */
function woo_cart_order_items( $item_data, $cart_item ) {
		
	foreach( $item_data as $key => $data ) {
		if( strpos( strtolower( $data['name'] ), 'manuscript file' ) !== false ) {
			unset( $item_data[$key] );
		}
	}
	return $item_data;
	
}
add_filter( 'woocommerce_get_item_data', 'woo_cart_order_items', 10, 2 );

/**
 * -----------------------------------------------------------
 * Checkout Order Details - Order Received Receipt page
 * 
 * Filter out fields from the order details
 * -----------------------------------------------------------
 */
function woo_order_details_meta_data( $html, $item, $args ) {
	
	$metadata = $item->get_formatted_meta_data();
	
	//check if on view-order page and get parameter is available
	if(isset($_GET['view-order'])) {
	    $order_id = $_GET['view-order'];
	}
	//check if on view order-received page and get parameter is available
	else if(isset($_GET['order-received'])) {
	    $order_id = $_GET['order-received'];
	}
	//no more get parameters in the url
	else {
	    $url = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	    $template_name = strpos($url,'/order-received/') === false ? '/view-order/' : '/order-received/';
	    if (strpos($url,$template_name) !== false) {
	        $start = strpos($url,$template_name);
	        $first_part = substr($url, $start+strlen($template_name));
	        $order_id = substr($first_part, 0, strpos($first_part, '/'));
	    }
	}
	
	foreach ( $metadata as $meta_id => $meta ) {
		
		if( strpos( strtolower( $meta->display_key ), 'manuscript file' ) === false ) {
			
			$value = $args['autop'] ? wp_kses_post( wpautop( make_clickable( $meta->display_value ) ) ) : wp_kses_post( make_clickable( $meta->display_value ) );
            $strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post( $meta->display_key ) . ':</strong> ' . $value;
			
		}
			
	}

 	if ( $strings ) {
 		$html = $args['before'] . implode( $args['separator'], $strings ) . $args['after'];
    }

	return $html;
	
}
add_filter( 'woocommerce_display_item_meta', 'woo_order_details_meta_data', 10, 3 );
