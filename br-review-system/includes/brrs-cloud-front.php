<?php

function cloudfront_url($document_url) {
	
	$private_key_filename = BRRS_PLUGIN_PATH . 'includes/pk-APKAJV2BXC5PIQL44L4Q.pem';
	$key_pair_id = 'APKAJV2BXC5PIQL44L4Q';
	$expires = strtotime('+1 day');
	
	// this policy is well known by CloudFront, but you still need to sign it, 
    // since it contains your parameters
    $canned_policy = '{"Statement":[{"Resource":"' . $document_url . '","Condition":{"DateLessThan":{"AWS:EpochTime":'. $expires . '}}}]}';
    
    // sign the canned policy
    $signature = rsa_sha1_sign($canned_policy, $private_key_filename);
    // make the signature safe to be included in a url
    $encoded_signature = url_safe_base64_encode($signature);
    
	return $document_url . '?Expires=' . $expires . '&Key-Pair-Id=' . $key_pair_id . '&Signature=' . $encoded_signature;
    
 }

function rsa_sha1_sign($policy, $private_key_filename) {
    $signature = "";

    // load the private key
    $fp = fopen($private_key_filename, "r");
    $priv_key = fread($fp, 8192);
    fclose($fp);
    $pkeyid = openssl_get_privatekey($priv_key);

    // compute signature
    openssl_sign($policy, $signature, $pkeyid);

    // free the key from memory
    openssl_free_key($pkeyid);

    return $signature;
 }

function url_safe_base64_encode($value) {
    $encoded = base64_encode($value);
    // replace unsafe characters +, = and / with 
    // the safe characters -, _ and ~
    return str_replace(
        array('+', '=', '/'),
        array('-', '_', '~'),
        $encoded);
 }

?>