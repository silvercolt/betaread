<?php

/**
 * ===================================
 * General Gravity Forms filters & Actions
 * ===================================
 */
 
 add_filter( 'gform_upload_path_1', 'change_upload_path', 10, 2 );
function change_upload_path( $path_info, $form_id ) {
	
	$path_info['path'] = get_home_path() . '/wp-content/uploads/query_letters/';
   	$path_info['url'] = content_url( 'uploads/query_letters/' );
   
    return $path_info;
   
}

add_action( 'gform_after_submission_2', 'brrs_create_review_report_metadata', 10, 2 );
function brrs_create_review_report_metadata($entry, $form) {
	
	$review_id = rgar( $entry, "76" );
	write_log("BRRS: Gold Report Submission for Review ID: " . $review_id . " and Entry ID: " . $entry['id']);
	$new_review_gold_report_entry_id = update_post_meta( $review_id, "brrs_gold_report_id", $entry['id'] );
	if(!$new_review_gold_report_entry_id) {
		write_log("BRRS: Failed to update the Gold Review Entry ID for Review " . $review_id);	
	}
	
}