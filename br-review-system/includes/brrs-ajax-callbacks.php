<?php

/*
 * ============================================================
 * Callback function for Review Actions
 * ============================================================
 */
add_action('wp_ajax_review_action', 'brrs_review_action_callback');

function brrs_review_action_callback() {

	$selected_action = $_REQUEST["review_action"];
	$review_id = $_REQUEST["review_id"];
	$selected_value = $_REQUEST["action_value"];
	$new_comment = $_REQUEST["comment"];
	$current_user_id = $_REQUEST["current_user_id"];
	$publisher_optin = $_REQUEST["publisher_optin"];
	
	$result['code'] = 0;
	$result['message'] = "Action: " . $selected_action . " failed to complete.";
	
	// Perform selected action
	$action_result = array();
	
	switch($selected_action) {
		case strtolower(BRRS_REVIEW_ACTIONS::assign) :
			$action_result = BRRS_REVIEW_ACTION_ASSIGN::assign_review( $review_id, $current_user_id, $selected_value );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::start) :
			$action_result = BRRS_REVIEW_ACTION_START::start_review( $review_id, $current_user_id );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::complete) : 
			$action_result = BRRS_REVIEW_ACTION_COMPLETE::complete_review( $review_id, $current_user_id, $selected_value, $new_comment );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::close) : 
			$action_result = BRRS_REVIEW_ACTION_CLOSE::close_review( $review_id, $current_user_id, $new_comment );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::pull) : 
			$action_result = BRRS_REVIEW_ACTION_PULL::pull_review( $review_id, $current_user_id, $new_comment );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::approve) :
			$action_result = BRRS_REVIEW_ACTION_APPROVE::approve_review( $review_id, $current_user_id );
			break;
		case strtolower(BRRS_REVIEW_ACTIONS::accept) :
			$action_result = BRRS_REVIEW_ACTION_ACCEPT::accept_review( $review_id, $current_user_id, $publisher_optin );
			break;
	}
	
	echo json_encode($action_result); 
	wp_die(); 
	
}

?>