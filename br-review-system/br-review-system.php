<?PHP
/**
 * Plugin Name: Beta|Read Review System
 * Plugin URI: http://betaread.com
 * Description: Establishes and Manages a Review System for submitted manuscripts 
 * 				either through configured WooCommerce products representing different 
 * 				levels of Review Services or by a System Administrator manually creating a new Review.
 * Version: 1.1.0
 * Author: SilverColt
 * Author URI: http://silvercolt.com
 * License: GPL2
 */
 
  /*  Copyright 2017  SilverColt  (email : webmaster@silvercolt.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/** 
 * --------------------------------------------
 * Check for required plugins and settings, inactivate if not found
 * --------------------------------------------
 */
add_action( 'admin_init', 'brrs_required_plugins_settings' );
function brrs_required_plugins_settings() {
	
    if ( is_admin() && 
    current_user_can( 'activate_plugins' ) &&
    /*
	 * WooCommerce
	 * @see https://woocommerce.com/ 
	 */
    (!is_plugin_active( 'woocommerce/woocommerce.php' ) ||
    /*
	 * WP Security Audit Log 
	 * @see https://www.wpsecurityauditlog.com/
	 */
    !is_plugin_active( 'wp-security-audit-log/wp-security-audit-log.php' ) ||
    /*
	 * User Role Editor
	 * @see https://www.role-editor.com/ 
	 */
    !is_plugin_active( 'user-role-editor/user-role-editor.php' ) ) ) {
        	
        add_action( 'admin_notices', 'brrs_required_plugin_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
		
    }
	//elseif() {
		
		//TODO: Check for expected review service products, without any review service products
		//		the Review System will not have a means of receiving manuscripts from customers.
		//		However, manually creating a new Review can be performed by a System Administrator.
		
	//}
}

function brrs_required_plugin_notice() {
    ?><div class="error">
    	<p>
    		Sorry, the Beta|Read Review System requires the following plugins to be installed and active. The BRRS has been deactivated.
    	</p>
    	<ul>
    		<li>WooCommerce</li>
    		<li>WP Security Audit Log</li>
    		<li>User Role Editor</li>
    	</ul>
    </div><?php
}

/** 
 * --------------------------------------------
 * Plugin Activation
 * --------------------------------------------
 */
register_activation_hook( __FILE__, 'brrs_on_activation' );
function brrs_on_activation() {

    // ATTENTION: This is *only* done during plugin activation hook
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
	BRRS_ROLES::add_roles();
	
}

/** 
 * --------------------------------------------
 * Plugin Deactivation
 * --------------------------------------------
 */
//register_deactivationn_hook( __FILE__, 'brrs_on_deactivation' );

/** 
 * --------------------------------------------
 * Plugin Constants 
 * --------------------------------------------
 */
 define( 'BRRS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
 define( 'BR_REVIEW_SYSTEM_PLUGIN_ACTIVE', true);
 define( 'BR_OPTION_REVIEW_PACKAGES', 'review_product_packages', true );
 

/** 
 * --------------------------------------------
 * Include Classes  
 * --------------------------------------------
 */
 include('classes/BRRS_ROLES.class.php');
 include('classes/BRRS_SETTINGS.class.php');
 include('classes/BRRS_ABOUT.class.php');
 include('classes/BRRS_REVIEW.class.php');
 include('classes/BRRS_REVIEW_STATUSES.class.php');
 include('classes/BRRS_REVIEW_ACTIONS.class.php');
 include('classes/BRRS_REVIEW_LEVELS.class.php');
 include('classes/BRRS_REVIEW_ACTION_ASSIGN.class.php');
 include('classes/BRRS_REVIEW_ACTION_START.class.php');
 include('classes/BRRS_REVIEW_ACTION_PULL.class.php');
 include('classes/BRRS_REVIEW_ACTION_APPROVE.class.php');
 include('classes/BRRS_REVIEW_ACTION_COMPLETE.class.php');
 include('classes/BRRS_REVIEW_ACTION_CLOSE.class.php');
 include('classes/BRRS_REVIEW_ACTION_ACCEPT.class.php');

/** 
 * --------------------------------------------
 * Included Actions
 * --------------------------------------------
 */
 include('init/brrs-review-post-type.php');
 include('init/brrs-review-meta-box-status.php');
 include('init/brrs-review-meta-box-manuscript.php');
 include('init/brrs-order-meta-box-review-details.php');
 include('init/brrs-review-taxonomy-upsell.php');
 include('init/brrs-review-taxonomy-genre.php');
 include('init/brrs-script-enqueuer.php');
 include('init/brrs-misc-actions.php');
 
/** 
 * --------------------------------------------
 * Included Filters
 * --------------------------------------------
 */
 include('init/brrs-misc-filters.php');
 
 /** 
 * --------------------------------------------
 * Basic Includes
 * --------------------------------------------
 */
 include('includes/brrs-ajax-callbacks.php');
 include('includes/brrs-logger.php');
 include('includes/brrs-woo-products.php');
 include('includes/brrs-woo-orders.php');
 include('includes/brrs-reviews-admin.php');
 include('includes/brrs-gravity-forms-file-upload.php');
 include('includes/brrs-gravity-forms.php');
 include('includes/brrs-download-package.php');
 include('includes/brrs-cloud-front.php');
 
 /**
  * =============================================
  * MISC Functions
  * =============================================
  */
  
  
 /** 
 * --------------------------------------------
 * Override WooCommerce Templates
 * --------------------------------------------
 */
add_filter( 'woocommerce_locate_template', 'woo_addon_plugin_template', 1, 3 );
   function woo_addon_plugin_template( $template, $template_name, $template_path ) {
     global $woocommerce;
     $_template = $template;
     if ( ! $template_path ) 
        $template_path = $woocommerce->template_url;
 
     $plugin_path  = untrailingslashit( plugin_dir_path( __FILE__ ) )  . '/templates/woocommerce/';
 
    // Look within passed path within the theme - this is priority
    $template = locate_template(
    array(
      $template_path . $template_name,
      $template_name
    )
   );
 
   if( ! $template && file_exists( $plugin_path . $template_name ) )
    $template = $plugin_path . $template_name;
 
   if ( ! $template )
    $template = $_template;

   return $template;
}