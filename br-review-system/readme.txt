=== BetaRead Review System ===
Contributors: silvercolt
Donate link: http://silvercolt.com
Tags: silvercolt,woocomerce,betaread
Requires at least: 4.8.1
Tested up to: 4.8.1
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Establishes and Manages a Review System for submitted manuscripts.

== Description ==

Establishes and Manages a Review System for submitted manuscripts 
either through configured WooCommerce products representing different 
levels of Review Services or by a System Administrator manually creating a new Review.

= Requirements =

* PHP version 5.3.3 or greater

== Installation ==

Follow these two options for installing this plugin

1. Upload `br-review-system` plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

OR

1. Upload the `br-review-system.zip` file within the 'Plugins' menu Upload option
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==



== Screenshots ==



== Changelog ==


= 1.1.0 =
* Change: Replaced manuscript metadata (title, genres, and manuscript file) for Gravity Forms Product Add-ons.
** Updated Review creation to capture the manuscript metadata from new order item metadata keys.
** Updated Review creation to attach the manuscript file from a gravity forms uploads path.
** Deleting Gravity Form entry  upon successful Order completion and Review creation.

= 1.0.0 =
* Initial release