<?php
/**
 * Registering custom alerts on all the different actions taken against a review
 * 
 * This file needs to be copied into the following wp-security-audit-log location:
 * /wp-content/uploads/wp-security-audit-log/custom-sensors/ 
 * 
 * @package Wsal
 * @subpackage Sensors
 * Support for BR-REVIEW-SYSTEM (BRRS) Plugin.
 *
 * 10000 Status: approved a review
 * 10001 Review assignee changed
 * 10002 Status: assigned
 * 10003 Status: completed
 * 10004 Status: closed a review
 * 10005 Status: pulled a review
 * 10006 Status: started a review
 * 10007 Review accreditation changed
 * 10008 Status: queued
 * 10009 Status: queued
 */
class WSAL_Sensors_brrs extends WSAL_AbstractSensor
{

    /**
     * Listening to events using WP hooks.
     */
    public function HookEvents()
    {
        
        add_action('updated_post_meta', array($this, 'ReviewAction'), 10, 4);

    }    

    /**
     * Trigger event when a review metadata is updated
     */
    public function ReviewAction( $meta_id, $post_id, $meta_key, $meta_value ) {
    	
		$event = null;
		$post = get_post($post_id);
		
		if (is_user_logged_in()) {
		
			if ($post->post_type = "review" ) {
					
				$event = $this->getEventTypeByMetaKey( $meta_key, $meta_value );
				if($event != null) {
					$editorLink = $this->GetEditorLink($post);
					$data = array(
						'PostID' => $post->ID,
						'PostTitle' => $post->post_title,
						'ReviewLink' => get_permalink($post->ID),
						$editorLink['name'] => $editorLink['value']
					);
					
					if($event === 10001) {
						$user = get_userdata($meta_value);
						$data['NewAssignee'] = $user->display_name;
					}
					
					if($event === 10007) {
						$data['AccreditationLevel'] = $meta_value;
					}				
					$this->plugin->alerts->Trigger($event, $data);
				}
			}
			
		}
		
    }
	
	/**
	 * Get event type by what metadata is being updated
	 * @param string $meta_key
	 * @param string $meta_value
	 * @return string $event
	 */
	 private function getEventTypeByMetaKey( $meta_key, $meta_value ) {
	 	
		$event = null;
		
		switch($meta_key) {
			case "brrs_status":
				$event = $this->getEventByDestinationStatusChange($meta_value);
				break;
			case "brrs_reviewer_id":
				$event = 10001;
				break;
			case "brrs_accreditation":
				$event = 10007;
				break;
		}
		
		return $event;
		
	 }
	 
	 /**
	 * Get event type by what status being changed to 
	 * @param string $meta_value
	 * @return string $event
	 */
	 private function getEventByDestinationStatusChange($meta_value) {
	 	
		$event = null;
		
		switch($meta_value) {
			case BRRS_REVIEW_STATUSES::approved: 
				$event = 10000;
				break;
			case BRRS_REVIEW_STATUSES::assigned: 
				$event = 10002;
				break;
			case BRRS_REVIEW_STATUSES::closed: 
				$event = 10004;
				break;
			case BRRS_REVIEW_STATUSES::complete: 
				$event = 10003;
				break;
			case BRRS_REVIEW_STATUSES::inProgress: 
				$event = 10006;
				break;
			case BRRS_REVIEW_STATUSES::pulled: 
				$event = 10005;
				break;			
			case BRRS_REVIEW_STATUSES::queued: 
				$event = 10008;
				break;	
			case BRRS_REVIEW_STATUSES::accepted: 
				$event = 10009;
				break;		
		}
		
		return $event;
		
	 }
	
	/**
	 * Get editor link.
	 * @param stdClass $post the post
	 * @return array $aLink name and value link
	 */
	private function GetEditorLink($post)
	{
		$name = 'EditorLink';
		$value = get_edit_post_link($post->ID);
		$aLink = array(
			'name' => $name,
			'value' => $value,
		);
		return $aLink;
	}

}
