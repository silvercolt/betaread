<?php

/**
 * 10000 Status: approved a review
 * 10001 Review assignee changed
 * 10002 Status: assigned
 * 10003 Status: completed
 * 10004 Status: closed a review
 * 10005 Status: pulled a review
 * 10006 Status: started a review
 * 10007 Review accreditation changed
 * 10008 Status: queued
 */
$custom_alerts = array(
    __('BRRS', 'wp-security-audit-log') => array(
        __('Actions', 'wp-security-audit-log') => array(
            array(10000, E_NOTICE, __('Review Status Changed to Approved', 'wp-security-audit-log'), __('Review Approved. Changed %PostTitle% status to <strong>Approved</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10001, E_NOTICE, __('Review Assignee Updated', 'wp-security-audit-log'), __('Review %PostTitle% Assigned to %NewAssignee%. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10002, E_NOTICE, __('Review Status Changed to Assigned', 'wp-security-audit-log'), __('Review Assigned. Changed %PostTitle% status to <strong>Assigned</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10003, E_NOTICE, __('Review Status Changed to Completed', 'wp-security-audit-log'), __('Review Completed. Changed %PostTitle% status to <strong>Completed</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10004, E_NOTICE, __('Review Status Changed to Closed', 'wp-security-audit-log'), __('Review Closed. Changed %PostTitle% status to <strong>Closed</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10005, E_NOTICE, __('Review Status Changed to Pulled', 'wp-security-audit-log'), __('Review Pulled. Changed %PostTitle% status to <strong>Pulled</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10006, E_NOTICE, __('Review Status Changed to Started', 'wp-security-audit-log'), __('Review Started. Changed %PostTitle% status to <strong>In Progress</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10007, E_NOTICE, __('Review Accreditation Updated', 'wp-security-audit-log'), __('Review %PostTitle% received the accreditation of <strong>%AccreditationLevel%</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10008, E_NOTICE, __('Review Status Changed to Queued', 'wp-security-audit-log'), __('Review Queued. Changed %PostTitle% status to <strong>Queued</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log')),
            array(10009, E_NOTICE, __('Review Status Changed to Accepted', 'wp-security-audit-log'), __('Review Accepted. Changed %PostTitle% status to <strong>Accepted</strong>. </br>Edit: %EditorLink%</br>View: %ReviewLink%', 'wp-security-audit-log'))
        )
    )	
 );