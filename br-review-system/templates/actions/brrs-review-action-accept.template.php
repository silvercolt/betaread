<script language="JavaScript">
	
	jQuery(document).ready(function($) {
    
	    // spinner on action dialog submit button
	    $( ".ui-dialog-buttonset > button[value='Accept Review']" ).on( "click", function() {
			$(this).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
	    });
    
   });
	
</script>

<div id="brrs-dialog-review-action-accept" title="Accept Accreditation">
	
	<div class="brrs-action-instructions">
		The following are optional as you Accept this Accreditation, <strong>please read carefully</strong> and make your selections.
	</div>
	
 	<div id="brrs-acceptance-options">
 		<div id="brrs-acceptance-option-optin">
	 		<h4>Publisher Opt-In</h4>
	 		<div class="brrs-acceptance-option">
	 			<label class="brrs-action-checkbox-label">Yes, notify Publishers of my recently accreditated manuscript.
				  <input class="brrs-action-checkbox" name="brrs-review-publisheroptin" id="brrs-review-publisheroptin" type="checkbox">
				  <span class="brrs-action-checkmark"></span>
				</label>
	 			<br/>
	 			<small>Beta|Read will share your contact information with the Partnered Publishers so they can contact you directly.</small>
	 		</div>
	 		<p>
	 			In order for us to pass your manuscript on to our partnering publishers for their review and consideration, please indicate your consent by checking the box above. 
	 		</p>
	 		<p>
	 			<i>If you choose to not give your consent, that is up to you.</i>
	 		</p>
	 	</div>
 		<div id="brrs-acceptance-option-query-letter" style="display:none;">
 			<?php if($now < strtotime("+7 day", strtotime($review_approved_date) ) ) { ?>
	 		<p>
	 			<strong>Should you give your consent</strong> to Beta|Read to pass on your manuscript, there is one more step we highly recommend prior to submission to editors or publishers. 
	 		</p>
	 		<p>
	 			<strong>The next step from here</strong> is for you to compose and attach a query letter to your manuscript giving propspective publishers an introduction to both yourself as an author, but also the submitted manuscript. 
	 		</p>
	 		<p>
	 			<u>It is highly recommended that you take this final step</u>, as a well composed query letter is an important part of the publishing process. 
	 			In the event you chose to not submit your query letter Beta|Read will simply make your manuscript available to the same publishers without the query letter.
	 		</p>
	 		<p>
	 			If you are unfamiliar with what a query letter is, <a style="text-decoration: underline; color: red;" href="/the-importance-of-a-query-letter/" target="_blank" title="Query Letter Information">please click here for more information</a>.
	 		</p>
	 		</p>
	 		<h4>Query Letter</h4>
	 		<div class="brrs-acceptance-option">
	 			<?php echo do_shortcode("[gravityform id=1 ajax=true field_values='review_id=" . $review_id ."' title=false description=false]"); ?>
	 		</div>
	 		<div class="brrs-action-warning">
	 			You have one week to write and attach a query letter.<br/>If you need more time, simply Cancel this action and come back later before <?php echo date('m/d/Y', strtotime("+7 day", strtotime($review_approved_date))); ?>.
	 		</div>
	 		<?php } ?>
	 	</div> 		
 	</div>
 		
 	<div style="text-align: center">
 		<strong>Thank you again for your hard work and for choosing Beta|Read. <br/>
 			We wish you the best with your manuscript moving forward.</strong>
 	</div>
  
</div>

<script type="text/javascript">
			// let's make call to the global gwrf variable visible after enabling Gravity Forms
			window.gwrf;

			(function ($) {

				gwrf = function (args) {
					// prototype arguments, created when we instantiate it
					this.formId = args.formId;
					this.spinnerUrl = args.spinnerUrl;
					this.refreshTime = args.refreshTime;
					this.refreshTimeout = null;
					
					// form wrapper class followed by the form id
					this.formWrapper = $('#gform_wrapper_' + this.formId);
					this.staticElem = this.formWrapper.parent();
					
					// we want to make sure that we'll have the cloned form html
					this.formHtml = $('<div />').append(this.formWrapper.clone()).html();

					this.spinnerInitialized = false;

					this.init = function () {

						var gwrf = this;
						
						// this is Gravity Forms .js hook which we bind a function call to
						// once the AJAX confirmation was loaded we'll trigger the function
						$(document).bind('gform_confirmation_loaded', function (event, formId) {
							
							// let's make sure we'll reload the right form
							if (formId != gwrf.formId || gwrf.refreshTime <= 0)
								return;
							
							// let's reload the form after some time
							gwrf.refreshTimeout = setTimeout(function () {
								gwrf.reloadForm();
							}, gwrf.refreshTime * 1000);

						});
						
						// let's make sure that the form will be closed only after the .closed-lightbox element was
						// clicked on. This might be a button in the top right corner of our popup
						$('.closed-lightbox').on('click', function (event) {
							event.preventDefault();
							gwrf.reloadForm();
						});

					};

					// heart of our functionality
					this.reloadForm = function () {
						
						// let's check if the confirmation message has already been created
						if (this.staticElem.find('.gform_confirmation_message_' + this.formId).length) {
							if (this.refreshTimeout)
								clearTimeout(this.refreshTimeout);
							
							// let's look for the confirmation element and get the HTML of the parent form
							this.staticElem.find('.gform_confirmation_message_' + this.formId)
									.wrap('<div />').parent().html(this.formHtml);
							// let's get rid of the HTML of the FORM
							this.staticElem.find('#gform_wrapper_' + this.formId).unwrap();
							
							// let's rerender the form
							$(document).trigger('gform_post_render', [this.formId, 0]);
							// if we had datepicker let's reinstantiate it
							if (window['gformInitDatepicker'])
								gformInitDatepicker();
						}
					};

					// utility used to display the "spinner" loading image
					this.initSpinner = function () {

						var gwrf = this;

						this.staticElem.on('submit', '#gform_' + this.formId, function () {
							$('#gform_submit_button_' + gwrf.formId).attr('disabled', true).after('<' + 'img id=\"gform_ajax_spinner_' + gwrf.formId + '\"  class=\"gform_ajax_spinner\" src=\"' + gwrf.spinnerUrl + '\" />');
							gwrf.formWrapper.find('.gform_previous_button').attr('disabled', true);
							gwrf.formWrapper.find('.gform_next_button').attr('disabled', true).after('<' + 'img id=\"gform_ajax_spinner_' + gwrf.formId + '\"  class=\"gform_ajax_spinner\" src=\"' + gwrf.spinnerUrl + '\" alt=\"\" />');
						});

					};

					this.init();

				};
				
				// if our document is ready we'll create a new form manually
				jQuery(document).ready(function () {
					new gwrf({"formId": 1, "spinnerUrl": "/wp-content/plugins/gravityforms/images/spinner.gif", "refreshTime": 0});
				});

			})(jQuery);

		</script>