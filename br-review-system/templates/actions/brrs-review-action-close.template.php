<script language="JavaScript">
	
	jQuery(document).ready(function($) {
    
	    // spinner on action dialog submit button
	    $( ".ui-dialog-buttonset > button[value='Close Review']" ).on( "click", function() {
			$(this).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
	    });
    
   });
	
</script>

<div id="brrs-dialog-review-action-close" title="Close Review">
	
	<p class="brrs-action-instructions">Please provide a reason for closing this review:</p>
 
 	<div id="brrs-review-comment-instructions">
    	<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> &nbsp;&nbsp;
    	All comments are public and viewable by the customer.
    </div>
	<label for="brrs-review-comment">Comment</label>
	<textarea name="brrs-review-comment" id="brrs-review-comment" rows="6" cols="20"></textarea>
  
</div>