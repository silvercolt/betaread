<script language="JavaScript">
	
	jQuery(document).ready(function($) {
    
	    // spinner on action dialog submit button
	    $( ".ui-dialog-buttonset > button[value='Complete Review']" ).on( "click", function() {
			$(this).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
	    });
    
 	});
	
</script>

<div id="brrs-dialog-review-action-complete" title="Complete Review">
	
	<p class="brrs-action-instructions">Please select the Accreditation Level and provide an optional comment on this accreditation decision.  Then complete the required Manuscript Review Report.</p>
 	
 	<label for="brrs-review-accreditation">Accreditation:</label>
	<select name="brrs-review-accreditation" id="brrs-review-accreditation">
	<option value="none">None</option>
	<?php
    foreach(BRRS_REVIEW_LEVELS::get_levels() as $level) {
    	if(strpos($review_package, "Bronze") !== false) {
    		if(strpos($level, "Level") === false) {
    			echo '<option value="' . $level . '">' . $level . '</option>';
    		}    		
    	}	
		elseif(strpos($review_package, "Bronze") === false) {
			if(strpos($level, "Level") !== false) {
    			echo '<option value="' . $level . '">' . $level . '</option>';
    		} 
		}				
	} ?>
	</select>
    </p>
    <div id="brrs-review-comment-instructions">
    	<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> &nbsp;&nbsp;
    	All comments are public and viewable by the customer.
    </div>
	<label for="brrs-review-comment">Accredition Comment <small>(This comment will show in the general list of review comments)</small></label>
	<textarea name="brrs-review-comment" id="brrs-review-comment" rows="6" cols="20"></textarea>
	
	<?php if(strpos($review_package, "Gold") !== false && empty( $review_gold_report_id ) ) { ?>
	<p>
		<h4>Manuscript Review Report</h4>
		<div id="brrs-complete-action-report">
			<?php echo do_shortcode("[gravityform id=2 ajax=true field_values='review_id=" . $review_id ."' title=false description=true]"); ?>
		</div>
  	</p>
  	<?php } elseif(!empty( $review_gold_report_id )) { ?>
  	<p>
		<h4>Manuscript Review Report</h4>
		<div id="brrs-complete-action-report">
			<strong>Report already completed.</strong> 
			<?php echo do_shortcode('[gravitypdf name="Gold Review Report" id="5a95fd96a7b5d" entry="' . $review_gold_report_id . '" text="Download Report"]'); ?>
		</div>
  	</p>
  	<?php } ?>
</div>