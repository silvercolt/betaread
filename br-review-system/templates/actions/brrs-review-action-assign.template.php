<script language="JavaScript">
	
	jQuery(document).ready(function($) {
    
	    // spinner on action dialog submit button
	    $( ".ui-dialog-buttonset > button[value='Assign Review']" ).on( "click", function() {
			$(this).prepend( "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i> &nbsp;&nbsp;" );
	    });
    
   });
	
</script>

<div id="brrs-dialog-review-action-assign" title="Assign Reviewer">
	
	<p class="brrs-action-instructions">Please select a Reviewer from the list below.</p>
 
	<label for="brrs-review-reviewer">Reviewer</label>
		<select name="brrs-review-reviewer" id="brrs-review-reviewer">';
		<?php 
			foreach(BRRS_REVIEW::get_reviewers() as $reviewer) {		
				echo '<option value="' . $reviewer->ID . '">' . $reviewer->first_name . " " . $reviewer->last_name . '</option>';	
			}
		?>
	</select>
  
</div>