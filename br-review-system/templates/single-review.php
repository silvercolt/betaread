<?php

/**
 * SINGLE REVIEW - TEMPLATE
 * ======================================================================================
 * Standard template for displaying a single Review post
 * (CPT = review)
 * 
 * 			------ BUSINESS RULES --------
 * The following roles are required and determine what
 * data is made visible and actions made available
 * 
 * 1) Administrator - Can view all Reviews and actions
 * 2) Auditor - Can view all Reviews, and limited actions
 * 3) Reviewer - Can only view of they are the Assignee of the Review, and limited actions
 * 4) Author - Can only view if they are the designated Author of the Review, no actions
 * 5) Publisher - Can only view if the review is complete, no actions
 * 
 * !!!NOTICE!!!!
 * COPY This to the currently active theme's folder for it to be effective
 * ======================================================================================
 * @author  SilverColt
 * @package br-review-system
 * @version 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); 

global $current_user;

?>
<div id="content">
		
	<?php 
	if ( BRRS_REVIEW::is_authorized( $post->ID, $current_user ) ) : 
		
		while( have_posts() ) : the_post();
		
			$review_id = get_the_ID();
			$review_package = get_post_meta( $review_id, "brrs_package", true );
			$review_package_order_id = get_post_meta( $review_id, "brrs_package_order_id", true ); 
			$review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
			$review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
			$review_approved_date = get_post_meta( $review_id, "brrs_approved_date", true );
			$review_publisher_optin = get_post_meta( $review_id, "brrs_publisher_optin", true );
			$now = time();
			$review_author_name = BRRS_REVIEW::get_author( $review_id )->first_name . " " . BRRS_REVIEW::get_author( $review_id )->last_name;
			$review_gold_report_id = get_post_meta( $review_id, "brrs_gold_report_id", true );
			
			// Reviewer
			if( is_object( BRRS_REVIEW::get_reviewer( $review_id ) ) ) {
				$reviewer_id = BRRS_REVIEW::get_reviewer( $review_id )->ID;
				$reviewer_name = BRRS_REVIEW::get_reviewer( $review_id )->first_name . " " . BRRS_REVIEW::get_reviewer( $review_id )->last_name;
				$reviewer_username = BRRS_REVIEW::get_reviewer( $review_id )->user_login;
			}
			else {
				$reviewer_name = BRRS_REVIEW::get_reviewer( $review_id );
				$reviewer_username = null;
			}
			// Review Accreditation
			if( empty( $review_accreditation ) || $review_accreditation === "none" ) {
				$review_accreditation = "None";
				$level = "LEVEL-0";
			}
			else {
				$level = str_replace(" ", "-", strtoupper($review_accreditation));
			}
			// Order Number
			if( empty( $review_package_order_id ) ) {
				$review_package_order_id = "Not Available";
			}
			if( BRRS_REVIEW::get_author( $review_id )->ID != get_current_user_id() ) {
				$order_url = "/wp-admin/post.php?post=" . $review_package_order_id . "&action=edit";
			}
			else {
				$order_url = "/my-account/view-order/" . $review_package_order_id;
			}
	?>
	
		<div id="brrs-single-review-breadcrumbs">
			<a href="/reviews" title="Back to Reviews">Back to Reviews</a>	
		</div>
		
		<div id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
		
			<div id="brrs-review-validate-message"></div>
			<h2><?php the_title(); ?></h2>
			<div id="brrs-single-review-submitted-date">Submitted on: <?php the_date(); ?> (Order #: <a href="<?php echo $order_url; ?>" title="Original Purchase Order"><?php echo $review_package_order_id; ?></a>)</div>
			<div id="brrs-single-review-submitted-by">By <?php echo BRRS_REVIEW::get_author( $review_id )->display_name; ?></div>
			
			<div id="brrs-single-review-status-container" class="brrs-<?php echo strtolower( str_replace( " ", "", BRRS_REVIEW::get_status( $post->ID ) ) ); ?>">
				<div id="brrs-accreditation-badge">
					<img src="http://www.betaread.com/wp-content/plugins/br-review-system/assets/badges/<?php echo $level; ?>.png" alt="Beta|Read Level Accreditation Badge"/>
				</div>
				<div id="brrs-single-review-meta">
					<div id="brrs-single-review-meta-col1">
						<table>
							<tr>
								<td class="review-summary-label">Status:</td>
								<td class="value">
									<?php echo BRRS_REVIEW::get_status( $review_id ); ?>
								</td>
							</tr>
							<tr>
								<td class="review-summary-label">Package:</td>
								<td class="review-summary-value"><?php echo $review_package; ?></td>
							</tr>
							<tr>
								<td class="review-summary-label">Author:</td>
								<td class="value"><a class="brrs-profile-link" href="/profile/<?php echo BRRS_REVIEW::get_author( $review_id )->user_login; ?>" title="View Author's Profile"><?php echo $review_author_name; ?></a></td>
							</tr>
						</table>
					</div>
					<div id="brrs-single-review-meta-col2">
						<table>
							<tr>
								<td class="review-summary-label">Genre(s):</td>
								<td class="value"><?php echo implode(",", $review_genres); ?></td>
							</tr>
							<tr>
								<td class="review-summary-label">Accreditation:</td>
								<td class="value"><?php echo $review_accreditation; ?></td>
							</tr>
							<tr>
								<td class="review-summary-label">Reviewer:</td>
								<?php if($reviewer_username == null) { ?>
									<td class="value"><?php echo $reviewer_name ?></td>	
								<?php } else { ?>
									<td class="value"><a class="brrs-profile-link" href="/profile/<?php echo $reviewer_username; ?>" title="View Reviewer's Profile"><?php echo $reviewer_name ?></a></td>
								<?php } ?>
							</tr>
							<?php if(
								BRRS_REVIEW::get_status( $review_id ) == BRRS_REVIEW_STATUSES::complete && 
								BRRS_REVIEW_ACTION_APPROVE::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) && 
								!empty( $review_gold_report_id ) &&
								strpos($review_package, "Gold") !== false) { ?>
								<tr>
									<td class="review-summary-label">Gold Report:</td>
									<td><?php echo do_shortcode('[gravitypdf name="Gold Review Report" id="5a95fd96a7b5d" entry="' . $review_gold_report_id . '" text="Download Report"]'); ?></td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>				
			</div>
			
			<!-- Review Actions -->
			<div id="brrs-single-review-actions" data-review="<?php the_ID(); ?>" data-user="<?php echo get_current_user_id() ?>">
				<?php if( BRRS_REVIEW_ACTION_APPROVE::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-approve" class="brrs-review-action" title="Approve the Review Results">Approve</button><?php endif; ?>
				<?php if( BRRS_REVIEW_ACTION_PULL::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-pull" class="brrs-review-action" title="Pull the Review">Pull</button><?php endif; ?>
				<?php if( BRRS_REVIEW_ACTION_CLOSE::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-close" class="brrs-review-action" title="Close the Review">Close</button><?php endif; ?>
				<?php if( BRRS_REVIEW_ACTION_ASSIGN::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-assign" class="brrs-review-action" title="Assign the Review">Assign</button><?php endif; ?>
				<?php if( BRRS_REVIEW_ACTION_START::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-start" class="brrs-review-action" title="Start the Review">Start</button><?php endif; ?>
				<?php if( BRRS_REVIEW_ACTION_COMPLETE::is_authorized(get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) ) : ?><button id="brrs-review-action-complete" class="brrs-review-action" title="Complete the Review">Complete</button><?php endif; ?>
			</div>
			
		<?php if(!empty($review_accreditation) && (BRRS_REVIEW::get_status( $review_id ) === BRRS_REVIEW_STATUSES::approved || BRRS_REVIEW::get_status( $review_id ) === BRRS_REVIEW_STATUSES::accepted )) { ?>
			
		<div id="brrs-single-review-accreditation-details-container">
			<div id="brrs-single-review-accreditation-details">
				<div id="brrs-accreditation-instructions">
					<?php if(strpos($review_accreditation, "3") !== false) { ?>
				 		<h3>Congratulations!</h3></br>
				 		Your manuscript has received the <strong>Beta</strong>|<strong style="color:red">Read</strong> <strong>Level 3</strong> designation!
				 		<p>
				 			Thank you for using Beta|Read! We have completed our review of your manuscript and have determined that it is of such quality as to warrant awarding our Level 3 designation. Your manuscript has met or exceeded our robust criteria and has also passed the mandatory review audit as well.
				 		</p> 
				 		<p>
				 			We are excited to see your manuscript be shared with the world.
				 		</p>
				 	<?php } else if(strpos($review_accreditation, "2") !== false) { ?>
				 		<h3>Thank you for submitting your manuscript to Beta|Read.</h3></br>
				 		<p>
				 			Your manuscript shows great promise, however, we do feel that some additional work will allow your manuscript to have the best opportunities in the very demanding market place. Accordingly we have assigned your manuscript a Level 2 accreditation. For more information about what this designation implies, please <a href="http://betaread.com/what-is-level-2">click here</a>.
						</p>
						<p>
							We encourage you to look at the services of a professional copy and content editor to help you hone your manuscript further. Again, your manuscript shows great promise and we feel that it is close to being submission ready. So please review the notes from our reviewer as a guide to help you take your manuscript to the next level 
						</p>
						<p>
							We look forward to reading the revised manuscript at some point in the future, as well as seeing it in a bookstore soon. Best wishes, and thank you again for using Beta|Read!
				 		</p>
				 	<?php } else if(strpos($review_accreditation, "1") !== false) { ?>
				 		<h3>Thank you for submitting your manuscript to Beta|Read.</h3></br>
				 		<p>
				 			We realize there is an emotional risk in seeking out the opinions of others about something that you have spent a great deal of effort to produce. We have completed a review of your submitted manuscript and have designated it to be a Level 1 manuscript. For more information on what this means please <a href="http://betaread.com/what-is-level-1">click here</a>.
						</p>
						<p>
							You clearly show a passion for writing, but we felt that your manuscript was missing certain fundamental qualities that would qualify it as submission-ready for publication. Our reviewers have made some notes that are attached to your review. Please consider those remarks as constructive criticism and a guide towards improving your writing skills. Also, visit our free resources on our Beta|Blog (insert link) to hints on how to hone the craft of writing and in applying those skills to your manuscript.
						</p>
						<p>
							Please don't stop learning and improving. We want you to succeed in your writing adventures. Keep improving, and check back in with in the future.
				 		</p>
				 	
				 	<?php } else if(strpos($review_accreditation, "Shelve It") !== false) { ?>
				 		<h3>Your Bronze Review has been completed</h3></br>
				 		<p>
                            We appreciate the opportunity to review the start of your manuscript. We recognize that there is more to your future book than the first 10,000 words, but from what we have this manuscript needs some notable adjustments and improvements prior to consideration for publication.
                        </p>
                        <p>
                            While it is not our intention to scare you away from continuing to pursue your story idea, it might be a valuable approach to put this manuscript on the shelf for a short time, perhaps a couple months, and revisit it again with a fresh set of eyes.
                        </p>
                        <p>
                            In the meantime, we encourage you to consider the notes left by our reviewer and reapply yourself to the craft of writing for a time, then come back to this manuscript with a renewed purpose.
                        </p>
                        <p>
                            Thank you again for using BetaRead, and we hope you will not only see the value in this feedback, but consider using us again in the future.
                        </p>
				 	
				 	<?php } else if(strpos($review_accreditation, "Pursue It") !== false) { ?>
				 		<h3>Your Bronze Review has been completed</h3></br>
				 		<p>
                            We appreciate the opportunity to review the start of your manuscript. We recognize that there is more to your future book than the first 10,000 words, but from what we have this manuscript shows good promise.
				 		</p>
				 		<p>
				 		    We recommend that you continue to pursue this manuscript to its completion. When the manuscript is finished, we hope that you will submit it to BetaRead again for our Silver or Gold reviews prior to submission for publication consideration. Our reviewer has added some additional noes for you to consider as you move forward.
				 		</p>
				 		<p>
				 		    Thank you again for using BetaRead, and we hope you will not only see the value in this feedback, but you will decide to send us the finished manuscript prior to submission with publishers.
				 		</p>

				 	<?php } ?>
				 	
				 	<?php if( 
				 		BRRS_REVIEW_ACTION_ACCEPT::is_authorized($review_id, get_current_user_id(),BRRS_REVIEW::get_status( $review_id )) && 
				 		BRRS_REVIEW::get_status( $review_id ) != BRRS_REVIEW_STATUSES::accepted && 
				 		strpos($review_accreditation, "Level") !== false) { ?>
				 	<div id="brrs-accreditation-actions">
				 		<h4>Final Step</h4>
				 		</br>
				 		Acceptance will unlock the final Review Package; additional options will be presented based on your accreditation level.
				 		</br>
				 		<button id="brrs-review-action-accept" class="brrs-review-action" title="Accept Review Accreditation">Accept</button>			 		
					</div>
					<?php } ?>
					
					<?php if(!empty($review_accreditation) && BRRS_REVIEW::get_status( $review_id ) === BRRS_REVIEW_STATUSES::accepted) { ?>
					<p>
						<h4>Review Accreditation Package</h4>
						<p>
							This document represents the review results, comments, and official statement from <strong>Beta</strong>|<strong style="color:red">Read</strong> regarding the accreditation awarded.
						</p>
						<form action="/wp-admin/admin-post.php" method="post" target="_blank">
							<input type="hidden" name="action" value="download_package">
  							<input type="hidden" name="review_id" value="<?php echo $review_id; ?>">
							<button type="submit" class="brrs-review-action" name="submit" title="Download Accreditation Package">Download</button>
						</form>
						
						<?php if(strpos($review_accreditation, "3") !== false && boolval($review_publisher_optin) ) { ?>
							<p>
								<h4>Publisher Opt-in</h4>
						 		<p>
						 			You have elected for Beta|Read to notify our partnered publishers about your Manuscript, 
						 			The above Accreditation Package has been made available to them. 
						 		</p>
						 	</p>
						<?php } ?>
					</p>
					<?php } ?>
				</div>
			</div>
		</div>
		
		<?php } ?>
			
		<div class="brrs-single-review-manuscript">
			<h3>Manuscript</h3>
			<p>
				<?php brrs_meta_box_review_manuscript(); ?>
			</p>
		</div>	
		
		<div id="brrs-single-review-dialogs">
			<?php 
				$action_templates = array(
					'assign'	=> BRRS_PLUGIN_PATH . '/templates/actions/brrs-review-action-assign.template.php',
					'complete'	=> BRRS_PLUGIN_PATH . '/templates/actions/brrs-review-action-complete.template.php',
					'close'		=> BRRS_PLUGIN_PATH . '/templates/actions/brrs-review-action-close.template.php',
					'accept'	=> BRRS_PLUGIN_PATH . '/templates/actions/brrs-review-action-accept.template.php'
				);
				foreach( $action_templates as $template_name => $template_path ) {
					if( file_exists( $template_path )) {
						include_once( $template_path );
					}
					else {
						echo "<!-- Failed to load Action Dialog: " . $template_name . " (path = ".$template_path.") -->";
					}
				}
			?>
		</div>
		
		<?php echo comments_template(); ?>
	
	<?php endwhile; ?>
	<?php else : ?>
			
		<h3>UNAUTHORIZED</h3>
		<p>
			We apologize, you have reached an area of the site which is restricted to <strong>registered</strong> and <strong>authorized</strong> members only. 
		</p>
		<p>
			If you feel you have reached this message in error please contact the <a href="contact-us" title="Contact Us">site administrator</a>
		</p>
		<p>
			Registered User? <a href="/profile">Login</a>
		</p>
	<?php endif; ?> 
	<?php wp_reset_query(); ?>
	
</div>

<?php get_footer();

// Omit closing PHP tag to avoid "Headers already sent" issues.
