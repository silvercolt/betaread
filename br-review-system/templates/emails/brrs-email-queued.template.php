<?php

/**
 * ===================================================
 * Trigger: Review has been purchased and/or queued
 * Purpose: advise auditor a review is ready for assignment
 * Recipient(s): Auditors
 * ===================================================
 * 
 * @author 		SilverColt
 * @package 	br-review-system/templates/emails
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 $review = get_post( $review_id ); 
 $review_title = get_the_title( $review_id );
 $review_author = BRRS_REVIEW::get_author($review_id)->display_name;;
 $review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );

?>
 <p>
 The following review has been queued, ready for Reviewer assignment.
 </p>
 <table>
 	<tbody>
 		<tr>
 			<td><strong>Title: </strong></td>
 			<td><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details"><?php echo $review_title; ?></a></td>
 		</tr>
 		<tr>
 			<td><strong>Author: </strong></td>
 			<td><?php echo $review_author; ?></td>
 		</tr>
 		<tr>
 			<td><strong>Genre(s): </strong></td>
 			<td><?php echo implode(", ", $review_genres); ?></td>
 		</tr>		
 	</tbody>
 </table>