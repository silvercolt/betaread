<?php

/**
 * ===================================================
 * Trigger: Review has been approved
 * Purpose: advise their review results have been approved
 * Recipient(s): Author (Customer)
 * ===================================================
 * 
 * @author 		SilverColt
 * @package 	br-review-system/templates/emails
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 $review = get_post( $review_id ); 
 $review_title = get_the_title( $review_id );
 $review_author = BRRS_REVIEW::get_author($review_id)->display_name;
 $review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
 $review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
?>
 <p>
 	The following review results have been <strong>Accepted</strong> by the author.  They will now be presented with their Review Accreditation Package.
 </p>
 <table>
 	<tbody>
 		<tr>
 			<td><strong>Title: </strong></td>
 			<td><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details"><?php echo $review_title; ?></a></td>
 		</tr>
 		<tr>
 			<td><strong>Author: </strong></td>
 			<td><?php echo $review_author; ?></td>
 		</tr>
 		<tr>
 			<td><strong>Genre(s): </strong></td>
 			<td><?php echo implode(", ", $review_genres); ?></td>
 		</tr>	
 		<tr>
 			<td><strong>Accreditation: </strong></td>
 			<td><?php echo $review_accreditation; ?></td>
 		</tr>	
 	</tbody>
 </table>
 <p>
 	<h3>Next Steps</h3>
 		<ul>
	 		<li><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details">Accept the Results</a>; receive a final Review Package with Accreditation Badge</li>
	 	<?php if(strpos($review_accreditation, "3") != false) { ?>
	 		<li>Allow Beta|Read to submit your review to Partnered Publishers</li>
	 		<strong>As you know, being submitted to a publisher is not a guarantee of being published.</strong>
	 		<p>
	 			We are constantly striving to develop new relationships with publishers all over the world to give you, the authors, your greatest opportunities in the marketplace.
			</p>
			<p>
				Receiving an endorsement from Beta|Read will help you a great deal in the process. Publishers anxiously await Level 3 manuscripts from Beta|Read, so it would not be surprising for publishers to start contacting you within the next month!
			</p>
			<p>
				As a reminder, Beta|Read is selective on the publishers with whom we partner. We also don't sell your information to any 3rd party company or organization. Our goal is to empower authors to succeed, not get scammed. So, feel free to <a href='http://betaread.com/contact'>contact us</a> if you have any questions, or to tell us the good news about your manuscript being published. 
			</p>
			<p>
				<strong>Best wishes from all of us at Beta|Read!</strong>
	 		</p>
	 	<?php } ?>
	 	</ul>
 </p>