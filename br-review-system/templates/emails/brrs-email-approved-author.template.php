<?php

/**
 * ===================================================
 * Trigger: Review has been approved
 * Purpose: advise their review results have been approved
 * Recipient(s): Author (Customer)
 * ===================================================
 * 
 * @author 		SilverColt
 * @package 	br-review-system/templates/emails
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 $review = get_post( $review_id ); 
 $review_title = get_the_title( $review_id );
 $review_author = BRRS_REVIEW::get_author($review_id)->display_name;
 $review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
 $review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
?>
 <p>
 	Your following review results have been <strong>Approved</strong>.
 	<?php if(strpos($review_accreditation, "3") != false) { ?>
 		<h2>Congratulations!</h2>
 		Your manuscript has received the <strong>Beta</strong>|<strong style="color:red">Read</strong> <strong>Level 3</strong> designation!
 		<p>
 			Thank you for using Beta|Read! We have completed our review of your manuscript and have determined that it is of such quality as to warrant awarding our Level 3 designation. Your manuscript has met or exceeded our robust criteria and has also passed the mandatory review audit as well.
 		</p> 
 		<p>
 			We are excited to see your manuscript be shared with the world.
 		</p>
 	<?php } else if(strpos($review_accreditation, "2") != false) { ?>
 		<p>
 			Thank you for submitting your manuscript to Beta|Read. Your manuscript shows great promise, however, we do feel that some additional work will allow your manuscript to have the best opportunities in the very demanding market place. Accordingly we have assigned your manuscript a Level 2 accreditation. For more information about what this designation implies, please <a href="http://betaread.com/what-is-level-2">click here</a>.
		</p>
		<p>
			We encourage you to look at the services of a professional copy and content editor to help you hone your manuscript further. Again, your manuscript shows great promise and we feel that it is close to being submission ready. So please review the notes from our reviewer as a guide to help you take your manuscript to the next level 
		</p>
		<p>
			We look forward to reading the revised manuscript at some point in the future, as well as seeing it in a bookstore soon. Best wishes, and thank you again for using Beta|Read!
 		</p>
 	<?php } else if(strpos($review_accreditation, "1") != false) { ?>
 		<p>
 			Thank you for submitting your manuscript to Beta|Read. We realize there is an emotional risk in seeking out the opinions of others about something that you have spent a great deal of effort to produce. We have completed a review of your submitted manuscript and have designated it to be a Level 1 manuscript. For more information on what this means please <a href="http://betaread.com/what-is-level-1">click here</a>.
		</p>
		<p>
			You clearly show a passion for writing, but we felt that your manuscript was missing certain fundamental qualities that would qualify it as submission-ready for publication. Our reviewers have made some notes that are attached to your review. Please consider those remarks as constructive criticism and a guide towards improving your writing skills. Also, visit our free resources on our Beta|Blog (insert link) to hints on how to hone the craft of writing and in applying those skills to your manuscript.
		</p>
		<p>
			Please don't stop learning and improving. We want you to succeed in your writing adventures. Keep improving, and check back in with in the future.
 		</p>
 	<?php } ?>
 </p>
 <table>
 	<tbody>
 		<tr>
 			<td><strong>Title: </strong></td>
 			<td><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details"><?php echo $review_title; ?></a></td>
 		</tr>
 		<tr>
 			<td><strong>Author: </strong></td>
 			<td><?php echo $review_author; ?></td>
 		</tr>
 		<tr>
 			<td><strong>Genre(s): </strong></td>
 			<td><?php echo implode(", ", $review_genres); ?></td>
 		</tr>	
 		<tr>
 			<td><strong>Accreditation: </strong></td>
 			<td><?php echo $review_accreditation; ?></td>
 		</tr>	
 	</tbody>
 </table>
 <p>
 	<h3>Next Steps</h3>
 		<ul>
	 		<li><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details">Accept the Results</a>; receive a final Review Package with Accreditation Badge</li>
	 	<?php if(strpos($review_accreditation, "3") != false) { ?>
	 		<li>Allow Beta|Read to submit your review to Partnered Publishers</li>
	 		<strong>As you know, being submitted to a publisher is not a guarantee of being published.</strong>
	 		<p>
	 			We are constantly striving to develop new relationships with publishers all over the world to give you, the authors, your greatest opportunities in the marketplace.
			</p>
			<p>
				Receiving an endorsement from Beta|Read will help you a great deal in the process. Publishers anxiously await Level 3 manuscripts from Beta|Read, so it would not be surprising for publishers to start contacting you within the next month!
			</p>
			<p>
				As a reminder, Beta|Read is selective on the publishers with whom we partner. We also don't sell your information to any 3rd party company or organization. Our goal is to empower authors to succeed, not get scammed. So, feel free to <a href='http://betaread.com/contact'>contact us</a> if you have any questions, or to tell us the good news about your manuscript being published. 
			</p>
			<p>
				<strong>Best wishes from all of us at Beta|Read!</strong>
	 		</p>
	 	<?php } ?>
	 	</ul>
 </p>