<?php

/**
 * ===================================================
 * Trigger: Review has been approved
 * Purpose: advise their review results have been approved
 * Recipient(s): Author (Customer)
 * ===================================================
 * 
 * @author 		SilverColt
 * @package 	br-review-system/templates/emails
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 global $userpro;
 $review = get_post( $review_id ); 
 $review_title = get_the_title( $review_id );
 $review_author = BRRS_REVIEW::get_author($review_id)->display_name;
 $review_author_id = BRRS_REVIEW::get_author($review_id)->ID;
 $review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
 $review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
?>
 <p>
 	<strong>Beta|Read would like to announce the latest manuscript to receive our coveted "Level 3" accreditation!</strong>
 </p>
 <p>
	As one of our select partners, you have been granted access to this exceptional manuscript. We have included the contact information for the author should you be interested in pursuing the manuscript further.
 </p>
 <p>
	We have found this manuscript to meet or exceed our criteria and consider it worthy of your time in review. We realize that not all manuscripts will fit your particular brand. But we believe this manuscript to be promising for interested publishers. 
 </p>
 <p>
	Please review the genre information below. Should you feel this manuscript is a potential match, click on the link provided, log in to your account, and you will be taken directly to the manuscript. 
 </p>
 <table>
 	<tbody>
 		<tr>
 			<td><strong>Title: </strong></td>
 			<td><a href="http://betaread.com/review/<?php echo $review->post_name;  ?>" title="Click to open the Review Details"><?php echo $review_title; ?></a></td>
 		</tr>
 		<tr>
 			<td><strong>Genre(s): </strong></td>
 			<td><?php echo implode(", ", $review_genres); ?></td>
 		</tr>
 		<tr>
 			<td><strong>Author: </strong></td>
 			<td><?php echo $review_author; ?></td>
 		</tr>
 		<tr>
 			<td valign="top"><strong>Contact: </strong></td>
 			<td>
 				Phone: <?php echo userpro_profile_data( "phone_number" , $review_author_id ); ?>
 				<br/>
 				Email: <?php echo userpro_profile_data( "user_email" , $review_author_id ); ?>
 			</td>
 		</tr>
 		<tr>
 			<td valign="top"><strong>Address: </strong></td>
 			<td>
 				<?php echo userpro_profile_data( "brrs_mailing_address_1" , $review_author_id ); ?><br/>
 				<?php echo userpro_profile_data( "brrs_mailing_address_2" , $review_author_id ); ?><br/>
 				<?php echo userpro_profile_data( "brrs_mailing_city" , $review_author_id ); ?>, <?php echo userpro_profile_data( "brrs_mailing_state" , $review_author_id ); ?> <?php echo userpro_profile_data( "brrs_mailing_postalcode" , $review_author_id ); ?><br/>
 				<?php echo userpro_profile_data( "brrs_mailing_country" , $review_author_id ); ?><br/>
 			</td>
 		</tr>
 	</tbody>
 </table>

 <?php if(!empty(BRRS_REVIEW_ACTION_ACCEPT::get_query_letter($review_id))) { ?>
	<div style="margin:10px; padding: 5px; text-align: center;"><strong>Note: See attached for Author's Query Letter</strong></div>	
 <?php } ?>
 
 <p>
	We want to remind you that there are no expectations, by any party, to publish a book that is "Level 3." Nor does Beta|Read represent the author as agent in any capacity. Additionally, there is no guarantee, implied or otherwise, by Beta|Read as to the market success of a manuscript with such accreditation. For all legal terms of service with respect to your account with Beta|Read in this regard, please click here. (insert link to our terms of service page). Best wishes in your publishing efforts! 
 </p>