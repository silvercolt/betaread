<?php

/**
 * ARCHIVE REVIEW - TEMPLATE
 * ======================================================================================
 * Standard template for displaying a list of Review posts
 * (CPT = review)
 * 
 * 			------ BUSINESS RULES --------
 * The following roles are required and determine what
 * data is made visible and actions made available
 * 
 * 1) Administrator - Can view all Reviews and actions
 * 2) Auditor - Can view all Reviews, and limited actions
 * 3) Reviewer - Can only view of they are the Assignee of the Review, and limited actions
 * 4) Author - Can only view if they are the designated Author of the Review, no actions
 * 5) Publisher - Can only view if the review is complete, no actions
 * 
 * !!!!NOTICE!!!!
 * COPY This to the currently active theme's folder for it to be effective
 * ======================================================================================
 * @author  SilverColt
 * @package br-review-system
 * @version 1.0.0
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
 
get_header(); 

global $current_user;

?>

<div id="content">

	<h1>My Reviews</h1>
	
	<?php if ( BRRS_REVIEW::is_authenticated( $current_user ) ) : ?>
	
	<table id="brrs-review-archives">
		<thead>
			<th>ID</th>
			<th>Title</th>
			<th>Author</th>
			<th>Genre(s)</th>
			<th>Accreditation</th>
			<th>Reviewer</th>
			<th>Package</th>
			<th>Status</th>
		</thead>
		<tbody>
	<?php 
	
		while( have_posts() ) : the_post(); 
	
		$review_id = get_the_ID();
		$review_package = get_post_meta( $review_id, "brrs_package", true );
		$review_package_order_id = get_post_meta( $review_id, "brrs_package_order_id", true ); 
		$review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
		$review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
		$review_author_name = BRRS_REVIEW::get_author( $review_id )->first_name . " " . BRRS_REVIEW::get_author( $review_id )->last_name;
		
		// Reviewer
		if( is_object( BRRS_REVIEW::get_reviewer( $review_id ) ) ) {
			$reviewer_name = BRRS_REVIEW::get_reviewer( $review_id )->first_name . " " . BRRS_REVIEW::get_reviewer( $review_id )->last_name;
			$reviewer_username = BRRS_REVIEW::get_reviewer( $review_id )->user_login;
		}
		else {
			$reviewer_name = BRRS_REVIEW::get_reviewer( $review_id );
			$reviewer_username = null;
		}
		// Review Accreditation
		if( empty( $review_accreditation ) ) {
			$review_accreditation = "None";
		}
		
		if ( BRRS_REVIEW::is_authorized( $review_id, $current_user ) ) :

	?>

		<tr id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<td><?php the_ID(); ?></td>
			<td><a class="brrs-review-link" href="<?php the_permalink(); ?>" title="View Review Details"><?php the_title(); ?></a></td>
			<td><a class="brrs-profile-link" href="/profile/<?php echo BRRS_REVIEW::get_author( $review_id )->user_login; ?>" title="View Author's Profile"><?php echo $review_author_name; ?></a></td>
			<td><?php echo implode(",", $review_genres); ?></td>
			<td><?php echo $review_accreditation; ?></td>
			<td>
				<?php if($reviewer_username == null) { ?>
					<?php echo $reviewer_name ?>
				<?php } else { ?>
					<a class="brrs-profile-link" href="/profile/<?php echo $reviewer_username; ?>" title="View Reviewer's Profile"><?php echo $reviewer_name ?></a>
				<?php } ?>
			</td>
			<td>
				<?php echo $review_package; ?>
			</td>
			<td>
				<div class="brrs-review-status brrs-<?php echo strtolower( str_replace( " ", "",  BRRS_REVIEW::get_status( $review_id ) ) ); ?>">
					<?php echo BRRS_REVIEW::get_status( $review_id ); ?>
				</div>
			</td>
		</tr>
		
	<?php endif; endwhile; ?>
	
		</tbody>
	</table>
	
	<?php wp_reset_query(); ?>
	
	<?php else : ?>
			
			<h3>UNAUTHORIZED</h3>
			<br/>
			We apologize but you have reached an area of the site which is restricted to registered AND authorized members only. 
			<br/>
			<br/>
			If you feel you have reached this message in error please contact the <a href="contact-us" title="Contact Us">site administrator</a>
			
	<?php endif; ?>
	
</div>

<?php get_footer();

// Omit closing PHP tag to avoid "Headers already sent" issues.