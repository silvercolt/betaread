<?php

/**
 * ===========================================
 * The Accreditation Package
 * A document which contains the details of a review and the manuscript
 * (title, author, genres) with a general statement of  the review's
 * final accreditation awarded, including any comments made during the review
 * an accreditation award badge, and the approval date.
 * 
 * This will include the Query Letter which may have been provided 
 * by the author during acceptance of this accreditation
 * ===========================================
 * 
 * @author 		SilverColt
 * @package 	br-review-system/templates/docs
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$review = get_post( $review_id ); 
$review_title = get_the_title( $review_id );
$review_author = BRRS_REVIEW::get_author($review_id)->display_name;
$review_genres = wp_get_post_terms( $review_id, "genre", array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'names') );
$review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
$level = str_replace(" ", "-", strtoupper($review_accreditation));
$review_approved_date = get_post_meta( $review_id, "brrs_approved_date", true );
$review_publisher_optin = get_post_meta( $review_id, "brrs_publisher_optin", true );
$query_letter = BRRS_REVIEW::get_query_letter($review_id);
$uploads = wp_upload_dir();
$logo = base64_encode(file_get_contents( $uploads['basedir'] .'/2018/01/Beta-Read-Logo.jpg'));
$badge = base64_encode(file_get_contents( BRRS_PLUGIN_PATH .'/assets/badges/' . $level .'.png'));
$comments = get_comments( "post_id=".$review_id );
$review_gold_report_id = get_post_meta( $review_id, "brrs_gold_report_id", true );

?>

<div style="margin-bottom:25px; text-align: center;">
	<img src="data:image/png;base64,<?php echo $logo; ?>" alt="Beta|Read Accreditation Award" width="600"/>
</div>

<div style="margin:10px; text-align: center;">
	<h1>Accreditation <?php echo $review_accreditation; ?></h1>
	For
	<br/>
	<h2><?php echo $review_title; ?></h2>
	By
	<br/>
	<h3><?php echo $review_author; ?></h3>
</div>

<div style="margin:25px 0 50px 0; text-align: center;">
	<?php if( strpos($level,"3") != false ) { ?>
		After having completed a review of the aforementioned manuscript it has been determined 
		that the manuscript is of such quality as to warrant awarding our <?php echo $review_accreditation; ?> 
		designation. Your manuscript has met or exceeded our robust criteria and has also passed the mandatory 
		review audit.
	<?php } if ( strpos($level,"2") != false ) { ?>
		
	<?php } if ( strpos($level,"1") != false ) { ?>
		
	<?php } ?>
</div>

<!-- Review Details -->
<div style="text-align: center;">
	<div style="float:right; margin-right: 50px; margin-top: 25px; text-align: left;">
		<table cellspacing="10" cellpadding="5">
			<tr>
				<td><strong>Approved:</strong></td>
				<td><?php echo $review_approved_date; ?></td>
			</tr>
			<tr>
				<td><strong>Publisher Opt-In:</strong></td>
				<td><?php echo ($review_publisher_optin) ? "Yes" : "No"; ?></td>
			</tr>
			<tr>
				<td><strong>Query Letter:</strong></td>
				<td><?php echo (!empty($query_letter)) ? "Yes" : "No"; ?></td>
			</tr>
			<tr>
				<td><strong>Genre(s):</strong></td>
				<td><?php echo implode(",", $review_genres); ?></td>
			</tr>
		</table>
	</div>
	<div style="float:left; margin-right:25px; margin-left:25px; text-align: center;">
		<img src="data:image/png;base64,<?php echo $badge; ?>" width="200"/>
	</div>
</div>

<!-- Review Comments -->
<div style="page-break-before: always;"></div>
<div>
	<h1>Review Comments</h1>
	<?php if(!empty($comments)) { foreach($comments as $comment) { ?>
		<div style="padding: 5px; margin:10px 0 10px 0;">
			<small><strong><?php echo $comment->comment_author . "</strong> on <span style='color: #ccc'>" . date('m/d/Y', strtotime($comment->comment_date)); ?></span></small>
			<p>
				<?php echo $comment->comment_content; ?>
			</p>
		</div>
	<?php } } ?>
</div>

<!-- Review Gold Report -->
<?php if(!empty($review_gold_report_id)) { ?>
<div style="page-break-before: always;"></div>
<div>
	<?php echo BRRS_REVIEW::get_gold_report($review_id); ?>
</div>

<!-- Query Letter added if available at the end -->
<?php } if(!empty(BRRS_REVIEW::get_query_letter($review_id))) { ?>
<div style="page-break-before: always;"></div>
<?php } ?>

<footer style="position:fixed; bottom:0;">
	<small>Document Created and Downloaded On <?php echo date('m/d/Y'); ?> from http://betaread.com</small>
</footer>