<?php

/*
 * ==========================================================
 * Meta Box for embedding the attached Manuscript PDF
 * ==========================================================
 */ 
function brrs_meta_box_review_manuscript() {
	
	global $post;
	
	$attachments = get_children( array (
                        'post_parent' 		=> $post->ID,
                        'post_type' 		=> 'attachment',
                        'post_mime_type' 	=> 'application/pdf',
                        'numberposts'		=> '1'
                    ));
	
	foreach( $attachments as $attachment ) {
		$attachment_url = wp_get_attachment_url( $attachment->ID );
		$manuscript_url = cloudfront_url($attachment_url);
		break;
	}	
	
	if( !$attachment_url ) {
		
		$content = <<<EOD
			<div id="$post->ID-manuscript" class="brrs-manuscript-container">
			  <div class="brrs-warning">
			  	<i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
			  	<div class="brrs-warning-message">Failed to load the attached manuscript; no manuscript found.</div>
			  </div>
			</div>
EOD;
		
	}
	else {
		$content = <<<EOD
			<div id="brrs-$post->ID-manuscript" class="brrs-manuscript-container">
			  <object  id="brrs-manuscript-content" width="400" height="700" type="application/pdf" data="$manuscript_url?#zoom=85&scrollbar=0&toolbar=0&navpanes=0">
			    <p>Please ensure you have Adobe Reader installed.  <a href="https://get.adobe.com/reader/" title="Download Adobe Reader" nofollow>Download Now.</a></p>
			  </object>
			</div>
EOD;
	}

	echo $content;
	
}