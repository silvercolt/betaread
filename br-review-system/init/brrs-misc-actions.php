<?php

/**
 * display default admin notice
 */ 
function brrs_add_settings_errors() {
	
    settings_errors();
    
}
add_action('admin_notices', 'brrs_add_settings_errors');

/**
 * default unlimited posts per page for reviews
 */
add_action('pre_get_posts','brrs_archive_posts_per_page');
function brrs_archive_posts_per_page($query){ 
    if ($query->is_main_query() && $query->is_archive && $query->query_vars['post_type'] == "review"){
        $query->set( 'posts_per_page', -1);
    }
    return $query;
}