<?php

/**
 * Disable PDF Thumbnails
 */
function wpb_disable_pdf_previews() { 
	$fallbacksizes = array(); 
	return $fallbacksizes; 
} 
add_filter('fallback_intermediate_image_sizes', 'wpb_disable_pdf_previews');