<?php

add_action( 'init', 'brrs_review_taxonomy_checkpoint_init', 0 );
function brrs_review_taxonomy_checkpoint_init() {

	$args = array(
	    'query_var' => true,
		'hierarchical' => true,
		'description' => 'List of Checkpoints required to be completed for the parent checkpoint (Review Package) ',
		'label' => __( 'Checkpoints', 'BRRS' ),
		'labels' => array( 
			'menu_name' => __( 'Checkpoints', 'BRRS' ),
			'add_new_item' => __( 'Add New Checkpoint', 'BRRS' ), 
			'edit_item' => __( 'Edit Checkpoint', 'BRRS' ),
			'update_item' => __( 'Update Checkpoint', 'BRRS' ),
			'all_items' => __( 'All Checkpoints', 'BRRS' ),
			'view_item' => __( 'View Checkpoint', 'BRRS' ),
			'new_item_name' => __( 'New Checkpoint Name', 'BRRS' ),
			'parent_item' => __( 'Parent Checkpoint', 'BRRS' ),
			'parent_item_colon' => __( 'Parent Checkpoint:', 'BRRS' ),
			'search_items' => __( 'Search Checkpoints', 'BRRS' ),
			'popular_items' => __( 'Popular Checkpoints', 'BRRS' ),
			'separate_items_with_commas' => __( 'Separate Checkpoints with commans', 'BRRS' ),
			'add_or_remove_items' => __( 'Add or Remove Checkpoints', 'BRRS' ),
			'choose_from_most_used' => __( 'Choose from the most used Checkpoints', 'BRRS' ),
			'not_found' => __( 'No Checkpoints found' )
			),
		'public' => true,
		'show_admin_column' => false,
		'rewrite' => array( 'slug' => 'checkpoint' ),
		'capabilities' => array(
			'assign_terms' => 'edit_posts',
			'edit_terms' => 'edit_posts',
			'manage_terms' => 'edit_posts',
			'delete_terms' => 'edit_posts'
		)
	);
	
	register_taxonomy( 'checkpoint', 'review', $args );
}