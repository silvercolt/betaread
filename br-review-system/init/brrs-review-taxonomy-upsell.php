<?php

add_action( 'init', 'brrs_review_taxonomy_upsell_init', 0 );
function brrs_review_taxonomy_upsell_init() {

	$args = array(
	    'query_var' => true,
		'hierarchical' => true,
		'description' => 'List of available Upsells that may come from purchasing the Review Package, requires workflow support for a Upsells listed',
		'label' => __( 'Upsells', 'BRRS' ),
		'labels' => array( 
			'menu_name' => __( 'Upsells', 'BRRS' ),
			'add_new_item' => __( 'Add New Upsell', 'BRRS' ), 
			'edit_item' => __( 'Edit Upsell', 'BRRS' ),
			'update_item' => __( 'Update Upsell', 'BRRS' ),
			'all_items' => __( 'All Upsells', 'BRRS' ),
			'view_item' => __( 'View Upsell', 'BRRS' ),
			'new_item_name' => __( 'New Upsell Name', 'BRRS' ),
			'parent_item' => __( 'Parent Upsell', 'BRRS' ),
			'parent_item_colon' => __( 'Parent Upsell:', 'BRRS' ),
			'search_items' => __( 'Search Upsells', 'BRRS' ),
			'popular_items' => __( 'Popular Upsells', 'BRRS' ),
			'separate_items_with_commas' => __( 'Separate Upsells with commans', 'BRRS' ),
			'add_or_remove_items' => __( 'Add or Remove Upsells', 'BRRS' ),
			'choose_from_most_used' => __( 'Choose from the most used Upsells', 'BRRS' ),
			'not_found' => __( 'No Upsells found', 'BRRS' )
			),
		'public' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'upsell' ),
		'capabilities' => array(
			'assign_terms' => 'edit_reviews',
			'edit_terms' => 'edit_reviews',
			'manage_terms' => 'edit_reviews',
			'delete_terms' => 'edit_reviews'
		)
	);
		
	register_taxonomy( 'upsell', 'review', $args );
}