<?php 

/** 
 * --------------------------------------------
 * Enqueue scripts 
 * --------------------------------------------
 */
function brrs_script_enqueuer() {
		
	wp_enqueue_script( 'jquery-ui-dialog' );
	wp_enqueue_script( 'jquery' );
	
	wp_register_script( "scripts", plugins_url('../assets/js/scripts.js', __FILE__), array('jquery') );
	wp_register_script( "datatables", plugins_url('../assets/datatables/datatables.min.js', __FILE__), array('jquery') );
	wp_register_script( "brrs-actions", plugins_url('../assets/js/brrs-actions.js', __FILE__), array('jquery') );
	wp_localize_script( 'scripts', 'ajax', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
	
	wp_enqueue_script( 'scripts' );
	wp_enqueue_script( 'datatables' );   
	wp_enqueue_script( 'brrs-actions' ); 
	
	wp_enqueue_style( 'wp-jquery-ui-dialog' );	
	wp_enqueue_style( 'datatables-style', plugins_url('../assets/datatables/datatables.min.css', __FILE__), false, '1.0.0', 'all');
	wp_enqueue_style( 'review-style', plugins_url('../assets/css/review-style.css', __FILE__), false, '1.0.0', 'all');
	wp_enqueue_style( 'font-awesome-style', plugins_url('../assets/font-awesome/css/font-awesome.min.css', __FILE__), false, '4.7.0', 'all');
}
add_action('init', "brrs_script_enqueuer", 10000);  