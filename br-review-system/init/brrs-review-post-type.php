<?php

/**
 * ------------------------------
 * Create new custom post type "Review"
 * ------------------------------
 */
add_action( 'init', 'brrs_create_review_post_type' );
function brrs_create_review_post_type() {
	register_post_type( 'review',
		array(
			'labels' => array(
				'name' => __( 'Reviews', 'BRRS' ),
				'menu_name' => __( 'Reviews', 'BRRS' ),
				'name_admin_bar' => __( 'Review', 'BRRS' ),
				'singular_name' => __( 'Review', 'BRRS' ),
				'add_new' => __( 'Add New', 'BRRS' ),
				'add_new_item' => __( 'Add New Review', 'BRRS' ),
				'edit_item' => __( 'Edit Review', 'BRRS' ),
				'new_item' => __( 'New Review', 'BRRS' ),
				'view_item' => __( 'View Review', 'BRRS' ),
				'search_items' => __( 'Search Reviews', 'BRRS' ),
				'not_found' => __( 'No Reviews Found', 'BRRS' ),
				'not_found_in_trash' => __( 'No Reviews found in Trash', 'BRRS' )
			),
			'public' => true,
			'has_archive' => 'reviews',
			'description' => 'A Manuscript Review',
			'exclude_from_search' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-visibility',
			'hierarchical' => false,
			'register_meta_box_cb' => 'brrs_add_review_meta_boxes',
			'supports' => array(
					'title',
					//'editor',
					'author',
					'revisions',
					'comments'
				),
			'rewrite' => array( 'slug' => 'review', 'feeds' => true ),
			'capabilities' => array(
				'publish_posts' => 'publish_reviews',
				'edit_posts' => 'edit_reviews',
				'edit_others_posts' => 'edit_others_reviews',
				'delete_posts' => 'delete_reviews',
				'delete_others_posts' => 'delete_others_reviews',
				'read_private_posts' => 'read_private_reviews',
				'create_posts' => 'create_reviews',
				'review_change_status' => 'review_change_status'
			),
			'map_meta_cap' => true
		)
	);
}

/**
 * ------------------------------
 * Review Post Type Help Context
 * ------------------------------
 */
add_action( 'contextual_help', 'brrs_review_add_help_text', 10, 3 );
function brrs_review_add_help_text( $contextual_help, $screen_id, $screen ) {
	
  //$contextual_help .= var_dump( $screen ); // use this to help determine $screen->id
  if ( $screen_id == 'lesson' || $screen_id == 'edit-lesson' ) {
	    $contextual_help = '<p>//TODO: PROVIDE GENERAL REVIEW GUIDANCE</p>';
  }
  return $contextual_help;
  
}

/*
 * --------------------------------------------
 * Meta Boxes for the Review Add/Edit Admin Page, 
 * (Callback used during Registering Custom Post Type)
 * --------------------------------------------
 */ 
function brrs_add_review_meta_boxes() {
    
	// Status Details	
    add_meta_box(
        'review_status_details', // $id
        __('Review Status','BRRS'), // $title 
        'brrs_meta_box_review_review_status', // $callback
        'review', // $page
        'side', // $context
        'high'// $priority
	); 
	
	//Embedded PDF
	add_meta_box(
		'review_manuscript', // $id
        __('Manuscript','BRRS'), // $title 
        'brrs_meta_box_review_manuscript', // $callback
        'review', // $page
        'top', // $context
        'high'// $priority
	);
	
	//Remove metaboxes from Review Edit Screen
	remove_meta_box( 'wpseo_meta', 'review', 'None' );
	remove_meta_box( 'commentstatusdiv', 'review', 'None' );
	remove_meta_box( 'slugdiv', 'review', 'None' );
	remove_meta_box( 'sharing_meta', 'review', 'None' );
	remove_meta_box( 'aam-acceess-manager', 'review', 'None' );
        
}

// Create 'top' section and move that to the top
add_action('edit_form_after_title', function() {
  global $post, $wp_meta_boxes;
  do_meta_boxes(get_current_screen(), 'top', $post);
  unset($wp_meta_boxes[get_post_type($post)]['top']);
});

/**
 * ====================================
 * Custom Post Type Template Redirect
 * ====================================
 */
add_filter('single_template', 'brrs_review_single_redirect');
function brrs_review_single_redirect($template){

	global $post;

     if ($post->post_type == 'review' ) {
          $template = BRRS_PLUGIN_PATH . '/templates/single-review.php';
     }
     return $template;
     wp_reset_postdata();
	 
}

add_filter('archive_template', 'brrs_review_archive_redirect');
function brrs_review_archive_redirect($template){

	if ( is_post_type_archive ( 'review' ) ) {
         $template = BRRS_PLUGIN_PATH . '/templates/archive-review.php';
    }
    return $template;
	
}