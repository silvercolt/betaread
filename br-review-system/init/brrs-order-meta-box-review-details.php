<?php

/*
 * ==========================================================
 * Order Admin Page Meta Box for Review Details: 
 * Reviewer, Status, and Accreditation management
 * ==========================================================
 */ 
function brrs_meta_box_order_show_review_details() {
	
	global $post;
	
	$order = wc_get_order( $post->ID );
	$items = $order->get_items();
	
	// Return fieldset for each item containing a review
	foreach( $items as $item_id => $item_data ) :
	
		$review_id = wc_get_order_item_meta( $item_id, "brrs_review_id", true );
		
		if( !empty( $review_id ) ) :
			
			$item_name = $item_data['name'];
			
			// Get item's Review Meta Data
			$reviewer_id = get_post_meta( $review_id, "brrs_reviewer_id", true );
			$status = get_post_meta( $review_id, "brrs_status", true );
			$accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
			
			// Check for negative conditions and set default values
			if( $reviewer_id == '0' || empty( $reviewer_id ) ) {
				$reviewer_name = "Unassigned";				
			}
			else {
				$reviewer = get_userdata( $reviewer_id );
				$reviewer_name = $reviewer->display_name;
			}
			
			if( empty( $status ) || $status == 'notset' ) {
				$status = "Not Set";
			}
			
			if( !in_array( $accreditation, BRRS_REVIEW_LEVELS::get_levels() ) ) {
				$accreditation = "None";
			}
			
			// Prepare Metabox Content
			$content = "<p><strong>$item_name</strong><br/>";
			$content .= "<div style='border-left: thin solid #ccc; padding-left: 5px; margin-top: 8px;'>";
			$content .= "Review ID: <a href='/wp-admin/post.php?post=$review_id&action=edit' title='Edit Review'>$review_id</a><br/>";
			$content .= "Reviewer: $reviewer_name<br/>";
			$content .= "Status: $status<br/>";
			$content .= "Accreditation: $accreditation";
			$content .= "</div></p>";
		
			echo $content;
		
		endif;
	
	endforeach;
	
}	