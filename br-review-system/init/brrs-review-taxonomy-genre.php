<?php

add_action( 'init', 'brrs_review_taxonomy_genre_init', 0 );
function brrs_review_taxonomy_genre_init() {

	$args = array(
	    'query_var' => true,
		'hierarchical' => true,
		'description' => 'List of Genres a manuscript could potentially be',
		'label' => __( 'Genres', 'BRRS' ),
		'labels' => array( 
			'menu_name' => __( 'Genres', 'BRRS' ),
			'add_new_item' => __( 'Add New Genre', 'BRRS' ), 
			'edit_item' => __( 'Edit Genre', 'BRRS' ),
			'update_item' => __( 'Update Genre', 'BRRS' ),
			'all_items' => __( 'All Genres', 'BRRS' ),
			'view_item' => __( 'View Genre', 'BRRS' ),
			'new_item_name' => __( 'New Genre Name', 'BRRS' ),
			'parent_item' => __( 'Parent Genre', 'BRRS' ),
			'parent_item_colon' => __( 'Parent Genre:', 'BRRS' ),
			'search_items' => __( 'Search Genres', 'BRRS' ),
			'popular_items' => __( 'Popular Genres', 'BRRS' ),
			'separate_items_with_commas' => __( 'Separate Genres with commas', 'BRRS' ),
			'add_or_remove_items' => __( 'Add or Remove Genres', 'BRRS' ),
			'choose_from_most_used' => __( 'Choose from the most used Genres', 'BRRS' ),
			'not_found' => __( 'No Genres found' )
			),
		'public' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'genre' ),
		'capabilities' => array(
			'assign_terms' => 'edit_reviews',
			'edit_terms' => 'edit_reviews',
			'manage_terms' => 'edit_reviews',
			'delete_terms' => 'edit_reviews'
		)
	);
	
	register_taxonomy( 'genre', 'review', $args );
	
}

/*
 * Add default genre terms, if none exist yet
 */
 add_action( 'init', 'brrs_default_genre_terms' );
 function brrs_default_genre_terms() {
 	
	$genres = get_terms( array( 
    	'taxonomy' => 'genre',
    	'parent'   => 0
	) ); 
	
	// Only add the defaults if there are no Genre terms already created
	if( empty( $genres ) ) {
		foreach( get_default_genres() as $term ) {
			wp_insert_term( $term, 'genre' );
		}		
	}
	
 }
 
 // Some basic default terms for Genre
function get_default_genres() {
	
	return array(
		
		'Drama',
		'Comedy',
		'Horror',
		'Realistic',
		'Romantic',
		'Satire',
		'Tragedy',
		'Tragicomedy',
		'Fantasy',
		'Mythology'
	
	);
	
}
