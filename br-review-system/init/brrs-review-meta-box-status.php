<?php

/*
 * ==========================================================
 * Meta Box for Package, Status, and Accreditation management
 * ==========================================================
 */ 
function brrs_meta_box_review_review_status() {
	
	global $post;
	
	// Readonly Metadata
	$reviewPackage = get_post_meta($post->ID, "brrs_package", true); // can not be changed except through the UPGRADE path
	$reviewPackageOrderId = get_post_meta($post->ID, "brrs_package_order_id", true); 
	$reviewQueryLetter = BRRS_REVIEW::get_query_letter($post->ID);
	
	// Editable Metadata
	$reviewReviewerId = get_post_meta($post->ID, "brrs_reviewer_id", true);
	$reviewStatus = get_post_meta($post->ID, "brrs_status", true);
	$reviewAccreditation = get_post_meta($post->ID, "brrs_accreditation", true);
	
	// Use nonce for verification
	echo '<input type="hidden" name="custom_lesson_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
    
	/******************
	 * Review Package *
	 ******************/
	echo '<p>';
	//TODO: make a link to the woocommerce product order page using $reviewPackageOrderId
    echo '<label for="review-package">Package:</label> ' . $reviewPackage . 
    	' (Order #<a href="/wp-admin/post.php?post=' . $reviewPackageOrderId . '&action=edit" title="Edit Order">' . $reviewPackageOrderId . '</a>)';
	echo '<br /><span class="description">The Purchased Review Package.</span>';
	echo '</p><p>';
	
	/******************
	 * Assigned Reviewer *
	 ******************/
	echo '<label for="review-reviewer">Reviewer:</label>';
    echo '<br>';
	echo '<select name="review-reviewer" id="review-reviewer">';
	if( !empty( $reviewReviewerId ) ) {
		echo '<option value="' . $reviewReviewerId . '" selected="selected">' . get_userdata( $reviewReviewerId )->display_name . '</option>';
		echo '<option value="0">Unassigned</option>';
	}
	else {
		echo '<option value="0" selected="selected">Unassigned</option>';
	}
	foreach(BRRS_REVIEW::get_reviewers() as $reviewer) {		
		echo '<option value="' . $reviewer->ID . '">' . $reviewer->first_name . " " . $reviewer->last_name . '</option>';	
	}
	echo '</select><br /><span class="description">The assigned Reviewer.</span>';
	echo '</p><p>';
	
	/******************
	 * Review Status *
	 ******************/
    echo '<label for="review-status">Status:</label>';
    echo '<br>';
	echo '<select name="review-status" id="review-status">';
	if( in_array( $reviewStatus, BRRS_REVIEW_STATUSES::get_statuses() ) ) {
			echo '<option value="' . $reviewStatus . '" selected="selected">' . $reviewStatus . '</option>';
	}
	else {
		echo '<option value="notset" selected="selected">Not Set</option>';
	}
    foreach(BRRS_REVIEW_STATUSES::get_statuses() as $status) {		
		echo '<option value="' . $status . '">' . $status . '</option>';	
	}	
	echo '</select><br /><span class="description">The current status of the Review.</span>';
    echo '</p><p>';	
    
    /*************************
	 * Review Accreditation *
	 *************************/
    echo '<label for="review-accreditation">Accreditation:</label>';
    echo '<br>';
	echo '<select name="review-accreditation" id="review-accreditation">';
	if( in_array( $reviewAccreditation, BRRS_REVIEW_LEVELS::get_levels() ) ) {
		echo '<option value="' . $reviewAccreditation . '" selected="selected">' . $reviewAccreditation . '</option>';
		echo '<option value="none">None</option>';
	} 
	else {
		echo '<option value="none" selected="selected">None</option>';
	}	
    foreach(BRRS_REVIEW_LEVELS::get_levels() as $level) {		
		echo '<option value="' . $level . '">' . $level . '</option>';	
	}	
	echo '</select><br /><span class="description">The final Review Accreditation Level.</span>';
    echo '</p>';
	
	/*************************
	 * Query Letter *
	 *************************/
	echo '<label for="review-query-letter">Query Letter:</label>';
    echo '<br>';
    if(!empty($reviewQueryLetter['filename'])) {
    	echo '<a href="' . $reviewQueryLetter['url'] .'" title="Download Query Letter" target="_blank">Download Query Letter</a>';

	} else {
		echo "No Letter Available";
	}	
}

/*
 * ---------------------------------
 * Save the Reviewer, Status, 
 * and Accreditation values
 * ---------------------------------
 */
add_action( 'save_post', 'brrs_save_review_status_meta' );
function brrs_save_review_status_meta( $post_id ) {
    
    // verify nonce
    if ( isset($_POST['review_meta_box_status_nonce']) && !wp_verify_nonce($_POST['review_meta_box_status_nonce'], basename(__FILE__))) 
        return $post_id;
    // check autosave
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;
    // check permissions
    if ( 'review' == $_POST['post_type'] ) {
        if (!current_user_can('edit_reviews', $post_id)) {
            return $post_id;
        } 
    }

	$reviewReviewerId = get_post_meta($post_id, "brrs_reviewer_id", true);
    $reviewStatus = get_post_meta($post_id, "brrs_status", true);
	$reviewAccreditation = get_post_meta($post_id, "brrs_accreditation", true);
	
	if(isset($_POST)) {
		$newReviewReviewerId = $_POST["review-reviewer"];
    	$newReviewStatus = $_POST["review-status"];
		$newReviewAccreditation = $_POST["review-accreditation"];
	
		// Save reviewer changes
	    if ($newReviewReviewerId && $newReviewReviewerId != $reviewReviewerId) {
	        update_post_meta($post_id, "brrs_reviewer_id", $newReviewReviewerId);
	    } elseif ('' == $newReviewReviewerId && $reviewReviewerId) {
	        delete_post_meta($post_id, "brrs_reviewer_id", $reviewReviewerId);
	    }
		
		// Save status changes
	    if ($newReviewStatus && $newReviewStatus != $reviewStatus) {
	        update_post_meta($post_id, "brrs_status", $newReviewStatus);
	    } elseif ('' == $newReviewStatus && $reviewStatus) {
	        delete_post_meta($post_id, "brrs_status", $reviewStatus);
	    }
		
		// Save accreditation changes	
		if ($newReviewAccreditation && $newReviewAccreditation != $reviewAccreditation) {
	        update_post_meta($post_id, "brrs_accreditation", $newReviewAccreditation);
	    } elseif ('' == $newReviewAccreditation && $reviewAccreditation) {
	        delete_post_meta($post_id, "brrs_accreditation", $reviewAccreditation);
	    }
    
    }

}