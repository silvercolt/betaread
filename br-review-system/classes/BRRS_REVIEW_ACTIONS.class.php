<?php

/**
 * ===================================================
 * Review ACTION enumerations 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTIONS {
	
    const assign = 'Assign';
	const approve = 'Approve';
	const start = 'Start';
	const complete = 'Complete';
	const close = 'Close';
	const pull = 'Pull';
	const accept = 'Accept';
	
	static public function get_actions() {
		
		$actions = array(
			self::assign,
			self::approve,
			self::start,
			self::complete,
			self::close,
			self::pull,
			self::accept
		);
		
		return $actions;
		
	}
	
}

if (!class_exists('BRRS_REVIEW_ACTIONS')) {
	 return new BRRS_REVIEW_ACTIONS;
 }