<?php

/**
 * ===========================================
 * Default Roles & Capabilities that will be having a part 
 * within the Review process. These roles will 
 * have different assigned capabilities
 * =============================================
 */
 
 class BRRS_ROLES {
 	
	/*
	 * ROLES
	 */
	const role_author = 'Author';
	const role_auditor = 'Auditor';
	const role_reviewer = 'Reviewer';
	const role_publisher = 'Publisher';
 	
	/*
	 * CAPABILITIES
	 */
	 
	// General
	const cap_create = 'create_reviews';
	const cap_edit = 'edit_reviews';
	const cap_read = 'read_reviews'; // read all reviews, no matter the status.
	const cap_delete = 'delete_reviews';
	const cap_publish = 'publish_reviews';
	const cap_edit_others = 'edit_others_reviews';
	const cap_delete_others = 'delete_others_reviews';
	const cap_read_private = 'read_private_reviews';
	
	// Read Review in Status
	const cap_read_review_status_queued = 'read_review_status_queued';
	const cap_read_review_status_approved = 'read_review_status_approved';
	const cap_read_review_status_assigned = 'read_review_status_assigned';
	const cap_read_review_status_inprogress = 'read_review_status_inprogress';
	const cap_read_review_status_complete = 'read_review_status_complete';
	const cap_read_review_status_closed = 'read_review_status_closed';
	const cap_read_review_status_pulled = 'read_review_status_pulled';
	
	// Change Review to Status
	const cap_review_change_status = 'review_change_status'; // Change to any status
	const cap_review_change_status_queued = 'review_change_status_queued';
	const cap_review_change_status_approved = 'review_change_status_approved';
	const cap_review_change_status_assigned = 'review_change_status_assigned';
	const cap_review_change_status_inprogress = 'review_change_status_inprogress';
	const cap_review_change_status_complete = 'review_change_status_complete';
	const cap_review_change_status_closed = 'review_change_status_closed';
	const cap_review_change_status_pulled = 'review_change_status_pulled';
	
	function add_roles() {	
		
		foreach( self::get_review_roles() as $role ) {
			//Create the Role				
			$result = add_role( strtolower( $role ), __( $role, 'BRRS' ) );
			// Log an Error if the adding the new role failed
			if( !empty( $result ) || !is_object( $result ) ) {
				error_log( "[BRRS] Failed to add new role: value = " . $role );
				add_action( 'admin_notices', function() { ?>
					<div class="notice notice-error is-dismissible">
						<p>The Beta|Read Review System failed to create the User Role: <?php echo $role; ?></p>
					</div>
					<?php }
     			);
			}
			// Add the Capabilities
			$new_role = get_role( strtolower( $role ) );
			foreach(self::get_review_capabilities( $role ) as $cap) {
				$new_role->add_cap( $cap );
			}
			
		}
		
	}
	
	function get_review_roles() {
		
		$roles = array(
			self::role_author,
			self::role_auditor,
			self::role_reviewer,
			self::role_publisher
		);
		
		return $roles;
		
	}
	
	function get_review_capabilities( $role = NULL ) {
		
		$capabilities = array();
		
		switch( $role ) {
			case self::role_author:
				$capabilities[] = self::cap_read;
				break;
			case self::role_auditor:
				$capabilities[] = self::cap_create;
				$capabilities[] = self::cap_edit;
				$capabilities[] = self::cap_read;
				$capabilities[] = self::cap_publish;
				$capabilities[] = self::cap_edit_others;
				$capabilities[] = self::cap_review_change_status;
				break;
			case self::role_reviewer:
				$capabilities[] = self::cap_edit;
				$capabilities[] = self::cap_read_review_status_assigned;
				$capabilities[] = self::cap_read_review_status_inprogress;
				$capabilities[] = self::cap_read_review_status_approved;
				$capabilities[] = self::cap_review_change_status_complete;
				break;
			case self::role_publisher:
				$capabilities[] = self::cap_read_review_status_complete;
				break;
			case NULL:
				$capabilities[] = self::cap_create;
				$capabilities[] = self::cap_edit;
				$capabilities[] = self::cap_read;
				$capabilities[] = self::cap_publish;
				$capabilities[] = self::cap_delete;
				$capabilities[] = self::cap_edit_others;
				$capabilities[] = self::cap_delete_others;
				$capabilities[] = self::cap_read_private;
				$capabilities[] = self::cap_read_review_status_queued;
				$capabilities[] = self::cap_read_review_status_approved;
				$capabilities[] = self::cap_read_review_status_assigned;
				$capabilities[] = self::cap_read_review_status_inprogress;
				$capabilities[] = self::cap_read_review_status_complete;
				$capabilities[] = self::cap_read_review_status_closed;
				$capabilities[] = self::cap_read_review_status_pulled;
				$capabilities[] = self::cap_review_change_status_queued;
				$capabilities[] = self::cap_review_change_status_approved;
				$capabilities[] = self::cap_review_change_status_assigned;
				$capabilities[] = self::cap_review_change_status_inprogress;
				$capabilities[] = self::cap_review_change_status_complete;
				$capabilities[] = self::cap_review_change_status_closed;
				$capabilities[] = self::cap_review_change_status_pulled;
				break;
		}
		
		return $capabilities;
		
	}

 }

if (!class_exists('BRRS_ROLES')) {
	 return new BRRS_ROLES;
 }
 