<?php

/**
 * ===================================================
 * Review Action: Pull Review
 * 
 * Pulling a review from being reviewed. Places in "Pulled" status
 * Indicating to the reviewer to stop reviewing until reassigned.
 * 
 * Note: Must first be assigned to a reviewer, auditor, or administrator
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - reviewer
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_PULL {
 	
	/**
	 * Start a review
	 * @param $review_id The review ID
	 * @param $current_user_id Current user's ID
	 * @return array
	 */
	static function pull_review( $review_id, $current_user_id ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to pull review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		
		if( self::is_authorized( $current_user_id ) ) {
		
			// Check if review is assigned before allowing it to be started.
			if( is_numeric( $current_assignee ) ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_status != BRRS_REVIEW_STATUSES::pulled ) {
					
					$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::pulled );
					$new_assignee_result = update_post_meta( $review_id, "brrs_reviewer_id", 0 );
						
					// Check if updates were successful		
					if( $new_status_result && $new_assignee_result ) {
						
						$result['code'] = 1;
						$result['message'] = 'Review successfully pulled.';	
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to pull review.';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully pulled';
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review must first be assigned.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor', 'reviewer' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::pulled
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Assignee - Advising they are assigned to perform the review
	 * - Author - General notice that their review has been assigned
	 * 
	 * @param $review_id
	 * @param $user_id
	 * @return boolean
	 */
	static function send_emails( $review_id, $user_id ) {
		
		
		
	}
	
 }

 if (!class_exists('BRRS_REVIEW_ACTION_PULL')) {
	 return new BRRS_REVIEW_ACTION_PULL;
 }