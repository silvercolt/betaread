<?php

/**
 * ===================================================
 * Review Action: Start Review
 * 
 * Starting the review process of a review.  
 * 
 * Note: Must first be assigned to a reviewer, auditor, or administrator
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - reviewer
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_START {
 	
	/**
	 * Start a review
	 * @param $review_id The review ID
	 * @param $current_user_id Current user's ID
	 * @return array
	 */
	static function start_review( $review_id, $current_user_id ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to start review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		
		if( self::is_authorized( $current_user_id ) ) {
		
			// Check if review is assigned before allowing it to be started.
			if( is_numeric( $current_assignee ) ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_status != BRRS_REVIEW_STATUSES::inProgress ) {
					
					// Update only if different
					if( $current_status != BRRS_REVIEW_STATUSES::inProgress ) {
						$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::inProgress );
					}
					else {
						$new_status_result = true;
					}
						
					// Check if updates were successful		
					if( $new_status_result ) {
						
						$result['code'] = 1;
						$result['message'] = 'Review successfully started';
						self::send_emails($review_id);
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to start review. [Status Update Failed]';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully started';
					self::send_emails($review_id);
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review must first be assigned.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor', 'reviewer' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::inProgress
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Author - General notice that their review has been assigned
	 * 
	 * @param $review_id
	 * @param $user_id
	 * @return boolean
	 */
	static function send_emails( $review_id ) {
		
		$subject = "💡 [BRRS] Review Started (" . $review_id . ")";	
		$author = BRRS_REVIEW::get_author($review_id);
		if(is_object($author)){
			$to_email = $author->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-started-author.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
 }

 if (!class_exists('BRRS_REVIEW_ACTION_START')) {
	 return new BRRS_REVIEW_ACTION_START;
 }