<?php

/**
 * ===================================================
 * Review Action: Accept Review's Resulting Accreditation
 * 
 * Accepting a review's final accreditation.
 * 
 * Note: Must first be in a APPROVED status AND Accreditation Level Assigned
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - author
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_ACCEPT {
 	
	/**
	 * Accept a review
	 * @param string $review_id The review ID
	 * @param string $current_user_id The current user
	 * @param boolean $publisher_optin whether the user has opted in for publisher notification
	 * @return array
	 */
	static function accept_review( $review_id, $current_user_id, $publisher_optin ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to accept review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
		
		if( self::is_authorized( $review_id, $current_user_id, $current_status ) ) {
		
			// Check if review is already accepted and in the approved status before allowing it to be started.
			if( !empty( $current_accreditation ) && $current_status == BRRS_REVIEW_STATUSES::approved ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_status != BRRS_REVIEW_STATUSES::accepted ) {
					
					// Update only if different
					if( $current_status != BRRS_REVIEW_STATUSES::accepted ) {
						$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::accepted );
						// Update publisher optin
						if(isset($publisher_optin)) {
							$new_publisher_optin = update_post_meta( $review_id, "brrs_publisher_optin", "1" );
						}
						else {
							$new_publisher_optin = update_post_meta( $review_id, "brrs_publisher_optin", "0" );
						}						
						// Create new attachment to Review for Query Letter if no attachment found already.
						if( !empty( get_attached_media( $review_id ) ) ) {
							self::attach_query_letter($review_id);
						}
					}
					else {
						$new_status_result = true;
					}
						
					// Check if updates were successful		
					if( $new_status_result ) {
						
						$result['code'] = 1;
						$result['message'] = 'Review successfully accepted.';
						self::send_emails($review_id);
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to accept review. [Status Update Failed]';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully accepted.';
					self::send_emails($review_id);
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review must first be Approved AND an Accreditation given.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $review_id, $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor', 'author' );
		$user_roles = $user->roles;
		$review_package = get_post_meta( $review_id, "brrs_package", true );
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::accepted &&
			strpos($review_package, "Bronze") === false	
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Assignee - Advising their review has been approved
	 * - Author - Final notice that their review has been approved
	 * 
	 * @param $review_id
	 * @param $user_id
	 * @return boolean
	 */
	static function send_emails( $review_id ) {
		
		// Send email to Author
		self::author_email($review_id);
		// Send email to Auditor
		self::auditor_email($review_id);
		// Send email to Publishers
		self::publisher_email($review_id);
		
	}
	
	static function author_email($review_id) {
		
		$subject = "💡 [BRRS] Review Accepted (" . $review_id . ")";	
		$author = BRRS_REVIEW::get_author($review_id);
		if(is_object($author)){
			$to_email = $author->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-accepted-author.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
	static function auditor_email( $review_id ) {
		
		$subject = "💡 [BRRS] Review Accepted (" . $review_id . ")";
		$bcc_emails = array();
		$users = get_users( array( 'role__in' => array ( 'auditor', 'administrator' ) ) );
		foreach($users as $user) {
			$bcc_emails[] = $user->user_email;
		}	
		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-accepted-auditor.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, "admin@betaread.com", $bcc_emails, $template);
		
	}
	
	static function publisher_email( $review_id ) {
		
		// Check if author opted in for publisher notification
		$review_publisher_optin = get_post_meta( $review_id, "brrs_publisher_optin", true );
		if( boolval($review_publisher_optin) ) {
			$subject = "💡 [BRRS] Level 3 Manuscript Released! (" . $review_id . ")";
			$bcc_emails = array();
			$users = get_users( array( 'role__in' => array ( 'publisher' ) ) );
			foreach($users as $user) {
				$bcc_emails[] = $user->user_email;
			}	
			$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-accepted-publisher.template.php";
			
			$attachments = array();
			$query_letter = BRRS_REVIEW::get_query_letter($review_id);
			if(!empty($query_letter)) {
				array_push( $attachments, $query_letter['path'] );
			}
			
			BRRS_REVIEW::send_emails($review_id, $subject, "admin@betaread.com", $bcc_emails, $template, $attachments);
		}		
		
	}

	static function attach_query_letter( $review_id ) {
			
		$query_letter = BRRS_REVIEW::get_query_letter($review_id);	
		
		// Check if a query letter has been uploaded for this review
		if(!empty($query_letter)) {
		
			// Check the type of file.
			$filetype = wp_check_filetype( basename( $query_letter ), null );
	
			$attachment = array(
				'guid'           => $query_letter["url"],
				'post_mime_type' => $query_letter['filetype']['type'],
				'post_title'     => $query_letter["title"],
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			
			$attach_id = wp_insert_attachment( $attachment, $query_letter, $review_id );
		
		}
		
	}
	
 }

 if (!class_exists('BRRS_REVIEW_ACTION_ACCEPT')) {
 	return new BRRS_REVIEW_ACTION_ACCEPT;
 }