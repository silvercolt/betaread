<?php

require_once(BRRS_PLUGIN_PATH . 'assets/dompdf/src/Autoloader.php');
require_once(BRRS_PLUGIN_PATH . 'assets/dompdf/lib/html5lib/Parser.php');
require_once(BRRS_PLUGIN_PATH . 'assets/Docx-to-HTML/docx_reader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
use Dompdf\Options;

class BRRS_REVIEW {
	
	/**
	 * Check if the current user is authorized access to the Review
	 * @param $review_id The review ID
	 * @param $current_user The wordpress user object
	 * @return Boolean
	 */
	static function is_authorized( $review_id, $current_user ) {
		
		$review = get_post( $review_id );
		$user_roles = $current_user->roles;
		$review_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );		
		$auth_user_roles = array( 'administrator', 'auditor', 'publisher', 'author' );
		$auth_user_ids = array( $review->post_author );
		if( is_object( BRRS_REVIEW::get_reviewer( $review_id ) ) ) {
			array_push( $auth_user_ids, BRRS_REVIEW::get_reviewer( $review_id )->ID );
		}
		// Authorization Check		
		if ( 0 !== $current_user->ID && !empty( array_intersect( $user_roles, $auth_user_roles ) ) ) {
			
			// Is the user the author?
			if( in_array( 'author', $user_roles ) && in_array( $current_user->ID, $auth_user_ids ) ) {
				return true;
			}
			// Is the user a publisher and is the review accreditation a level 3?
			elseif( ( in_array( 'publisher', $user_roles ) && 
					$review_accreditation == BRRS_REVIEW_LEVELS::level3 ) ) {
				return true;
			}
			// Is the user an Admin or Auditor
			elseif( in_array( 'administrator', $user_roles ) || in_array( 'auditor', $user_roles ) ) {
				return true;
			}
			// Is the user the assigned reviewer?
			elseif( in_array( 'reviewer', $user_roles ) && in_array( $current_user->ID, $auth_user_ids ) ) {
				return true;
			}
			else {
				return false;
			}
			
		}
		else {
			return false;
		}		
		
	}
	
	/**
	 * Check if the current user is authenticatd to view anything regarding reviews?
	 * @param $review The review ID
	 * @return Boolean
	 */
	static function is_authenticated( $current_user ) {
		
		$user_roles = $current_user->roles;
		$auth_user_roles = array( 'administrator', 'auditor', 'publisher', 'author' );
		
		if ( 0 !== $current_user->ID && !empty( array_intersect( $user_roles, $auth_user_roles ) ) ) {
			return true;
		}
		else {
			return false;
		}		
		
	}
	
	/**
	 * Get all users with the role "reviewer"
	 * @return Array of wordress users
	 */
	static function get_reviewers() {
		
		return get_users( array( 'role_in' => array( 'reviewer' ) ) );
		
	}
	
	/**
	 * Get the reviewers's user object
	 * @return Object Reviewer User Object
	 */
	static function get_reviewer( $review_id ) {
		
		$reviewer_id = get_post_meta( $review_id, "brrs_reviewer_id", true );
		if( !$reviewer_id ) {
			return 'Not Assigned';
		}
		else {
			return get_userdata( $reviewer_id );
		}
		
	}
	
	/**
	 * Get the reviewers's display name
	 * @return Object Author User Object
	 */
	static function get_author( $review_id ) {
		
		$author_id = get_post_field( 'post_author', $review_id );
		if( !$author_id ) {
			return 'Not Assigned';
		}
		else {
			return get_userdata( $author_id );
		}
		
	}
	
	/**
	 * Get review status
	 * @return String review status
	 */
	static function get_status( $review_id ) {
		
		$review_status = get_post_meta( $review_id, "brrs_status", true );
		if( empty( $review_status ) || $review_status == 'notset' ) {
			return "Not Set";
		}
		else {
			return $review_status;
		}
		
	}
	
	/**
	 * Create new Review from an Order
	 * @param $order the WooComerce order object
	 * @return $review_id the post ID of the new Review, otherwise FALSE
	 */
	 static function create_review( $order_id, $order ) {
		
		$results = NULL;
		
		$order_items = $order->get_items();
		
		foreach( $order_items as $item_id => $item_data ) :
			
			if( in_array( wc_get_order_item_meta( $item_id, '_product_id', true ), get_option( BR_OPTION_REVIEW_PACKAGES ) ) ) :
				
				// Gather order meta
				$manuscript_title = wc_get_order_item_meta( $item_id, 'Manuscript Title', true );
				$manuscript_link = wc_get_order_item_meta( $item_id, 'Manuscript File', true );
				$primary_genre = wc_get_order_item_meta( $item_id, 'Primary Genre', true );
				$primary_genre_slug = strtolower(str_replace( ' ', '-', $primary_genre ) );
				$secondary_genre = wc_get_order_item_meta( $item_id, 'Secondary Genre', true );
				$secondary_genre_slug = strtolower(str_replace( ' ', '-', $secondary_genre ) );
				$status = BRRS_REVIEW_STATUSES::queued;
				
				// Check if the genres submitted on checkout exist in the Review's Taxonomy
				// If not, add them.
				$term_library = get_terms( 'genre', array( 'hide_empty' => false ) );
				foreach( $term_library as $term ) {
					$terms[] = strtolower($term->name);
				}
				// Check if Primary Genre is available
				if( !in_array( strtolower($primary_genre), $terms ) ) {
					wp_insert_term( $primary_genre, 'genre' );
				}
				// Check if Secondary Genre is available
				if( !in_array( strtolower($secondary_genre), $terms ) ) {
					wp_insert_term( $secondary_genre, 'genre' );
				}
				
				$review_args = array(
				  'post_title'		=> wp_strip_all_tags( $manuscript_title ),
				  'post_type'		=> 'review',
				  'post_status'		=> 'publish',
				  'post_author'		=> $order->get_user_id(),
				  'meta_input'		=> array( 
				  		'brrs_package' 			=> $item_data['name'],
				  		'brrs_package_order_id' => $order->get_order_number(),
				  		'brrs_reviewer_id' 		=> null,
				  		'brrs_accreditation' 	=> null
					)
				);
				 
				// Insert the new review post and add the Genre terms
				$review_id = wp_insert_post( $review_args );
				
				// Check if review was created successfully, if so add some meta data and attachments
				if( !is_wp_error( $review_id ) ) {
					
					$results[$item_id] = $review_id;
					wp_set_object_terms( $review_id, array( $primary_genre_slug, $secondary_genre_slug ), 'genre', true );
					add_post_meta( $review_id, 'brrs_package_order_id', $order_id );
					add_post_meta( $review_id, 'brrs_status', BRRS_REVIEW_STATUSES::queued );
					
					// Add system comment to Review
					$comment_data = array(
					    'comment_post_ID' => $review_id,
					    'comment_author' => 'System',
					    'comment_author_email' => 'noreply@betaread.com',
					    'comment_content' => 'The Review is ready for assignment!',
					    'comment_type' => '',
					    'comment_parent' => 0,
					    'user_id' => $order->get_user_id(),
					    'comment_date' => current_time('mysql'),
					    'comment_approved' => 1,
					);				
					wp_insert_comment($comment_data);
					
					//Grab order's manuscript and attach to the Review
					$a = new SimpleXMLElement( $manuscript_link );
					$manuscript_url = $a['href'];
					$manuscript_path = str_replace("https://www.betaread.com/wp-content/uploads/", '', $manuscript_url);
					$manuscript_path_pieces = explode("/", $manuscript_path );
					$manuscript_file_name = $manuscript_path_pieces[4];
					$filename = basename ( $manuscript_file_name );
					$attachment_args = array(
						'post_title'		=> $manuscript_title,
						'post_name'			=> $filename,
						'post_content'		=> '',
						'post_status'		=> 'publish',
						'post_mime_type'	=> 'application/pdf'
					);

					$uploads_dir = wp_upload_dir();
					$review_manuscript_id = wp_insert_attachment( $attachment_args, $uploads_dir['basedir'] . '/' . $manuscript_path, $review_id );
					$attach_data = wp_generate_attachment_metadata( $review_manuscript_id, $uploads_dir['basedir'] . '/' . $manuscript_path );
					
					// Trigger the S3 Offloader to move the documents to the S3 Bucket
					$review_attachment_update = wp_update_attachment_metadata( $review_manuscript_id, $attach_data );					
					
					if( !$review_manuscript_id ) {
						write_log( 'BRRS: Failed to attach manuscript to review: ' . $review_id );
					}
					
					// Send email notifications
					$subject = "💡 [BRRS] New Review Submitted (" . $review_id . ")";
					$bcc_emails = array();
					$users = get_users( array( 'role__in' => array ( 'auditor', 'administrator' ) ) );
					foreach($users as $user) {
						$bcc_emails[] = $user->user_email;
					}	
					$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-queued.template.php";
					$emails_sent = self::send_emails($review_id, $subject, null, $bcc_emails, $template);
					
					if(!$emails_sent){
						write_log( 'BRRS: Failed to send Queued notification for Review ID ' . $review_id );
					}								
					
				}
				else {  // If review was not successfully created
					
					write_log( 'BRRS: ' . $review_id->get_error_message() );
					$results[$item_id] =  FALSE;
					
				}
			
			else :
				
				write_log( "BRRS: Order Received. No review items were found." );
					
			endif; // If ordered item is a review package
			
		endforeach; // Order items loop
		
		return $results;
		
	 }
	
	/**
	 * Send provided notification about the review
	 * 
	 * @param $review_id
	 * @param $subject
	 * @param $to_email
	 * @param $bcc_emails
	 * @param $template
	 * @return boolean
	 */
	static function send_emails($review_id, $subject, $to_email, $bcc_emails, $template, $attachments = array()) {
		
		$sent = false;
		$message = "Sorry, something went wrong.";
		if(empty($to_email)) {
			$to_email = 'admin@betaread.com';
		}
		
		// Set Headers
		$headers = array(
			"From: Beta|Read <admin@betaread.com>" . "\r\n",
			"Content-Type: text/html; charset=UTF-8"
		);	
		
		if(!empty($bcc_emails)) {
			array_push($headers, 'Bcc: ' . implode(',',$bcc_emails));
		}

		if( file_exists( $template )) {
			ob_start();
			require_once( $template );
			$message = ob_get_clean();
		}

		// Send the email
		if(!empty($to_email)) {
			$sent = wp_mail($to_email, $subject, $message, $headers, $attachments);
			if(!$sent){
				write_log( 'BRRS: Failed to send '.$subject.' notification for Review ID ' . $review_id );
			}
		}
		else {
			write_log( 'BRRS: Failed to send '.$subject.' notification for Review ID ' . $review_id . ' due to no email address found');
		}
		
		return $sent;
		
	}
	
	/**
	 * Get the review's query letter, if available
	 * returns empty array if not letter found
	 * @param $review_id string the review's ID
	 * @return $query_letter array()
	 */
	static function get_query_letter( $review_id ) {
		
		$query_letter = array();
		$letter = null;
		$dir = ABSPATH . 'wp-content/uploads/query_letters/';
		$files = scandir($dir);
		if(is_array($files)) {
			foreach ($files as $file) {
				if(strpos($file, $review_id) != false) {
					$letter = $file;
					break;
				}
			} 
			
			$query_letter["path"] 		= $dir . $letter;
			$query_letter["title"] 		= preg_replace( '/\.[^.]+$/', '', basename( $letter ) );
			$query_letter["filename"] 	= $letter;
			$query_letter["filetype"]	= wp_check_filetype( basename( $letter ), null );
			$query_letter["url"]		= content_url( 'uploads/query_letters/' ) . $letter;
			
		}
		
		return $query_letter;
		
	}

	static function get_accreditation_package( $review_id ) {
		
		$query_letter = BRRS_REVIEW::get_query_letter($review_id);
		$package = null;
		$ql_html = null;		
		
		ob_start();
		$doc_template = BRRS_PLUGIN_PATH . 'templates/docs/brrs-review-doc-accreditation-package.template.php';
		
		if( file_exists( $doc_template )) {
			include_once( $doc_template );
			$package = ob_get_clean();
		}
		else {
			ob_end_clean();
			write_log("Failed to load Doc Template: Accreditation Package (path = ".$doc_template.")");
		}
		
		if(!empty($package)) {
			
			if(!empty($query_letter)) {

				$doc = new Docx_reader();
				$doc->setFile($query_letter['path']);
				if(!$doc->get_errors()) {
				    $ql_html = $doc->to_html();
				} else {
				    write_log("BRRS: Failed to convert Query Letter to HTML for review " .$review_id . "[" . $doc->get_errors() . "]");
				}
				
			}
			
			$options = new Options();
			$options->set('defaultFont', 'Courier');
			$options->set('isRemoteEnabled', TRUE);
			$options->set('debugKeepTemp', TRUE);
			$options->set('isHtml5ParserEnabled', true);
			$dompdf = new Dompdf($options);
			$dompdf->setBasePath(ABSPATH . '/wp-content/uploads');
			$dompdf->load_html($package . $ql_html);
			$dompdf->render();
			$dompdf->stream("betaread-accreditation-package-" . $review_id . ".pdf");
		
		}
		
	}

	static function get_accreditation_badge( $accredition ) {
			
		$badge = array();
		$my_badge = null;
		$dir = BRRS_PLUGIN_PATH . 'assets/badges/';
		$files = scandir($dir);
		if(is_array($files)) {
			foreach ($files as $file) {
				if(strpos($file, strtoupper(str_replace(" ", "-", $accredition))) !== false) {
					$my_badge = $file;
					break;
				}
			} 
			
			$badge["path"] 		= $dir . $my_badge;
			$badge["title"] 		= preg_replace( '/\.[^.]+$/', '', basename( $my_badge ) );
			$badges["filename"] 	= $my_badge;
			$badge["filetype"]	= wp_check_filetype( basename( $my_badge ), null );
			$badge["url"]		= content_url( 'assets/badges/' ) . $my_badge;
			
		}
		
		return $badge;		
			
	}
	
	/**
	 * Get the review's gold report
	 * @param $review_id 
	 * @return $gold_report HTML of report
	 */
	static function get_gold_report( $review_id ) {
			
		$entry_id = get_post_meta( $review_id, "brrs_gold_report_id", true );
		$gold_report = array();
		
		$entry = RGFormsModel::get_lead($entry_id);
		$form = RGFormsModel::get_form_meta($entry['form_id']);
		
		$gold_report =  '<h1>'.$form['title'].'</h1>';
		$gold_report .=  '<div>'.$form['description'].'</div><p>';
		
		foreach($form['fields'] as &$field){
			
			if($field['type'] == 'section') {
				$gold_report .= '<div style="width:100%; background-color: #999; padding: 5px; margin: 20px 0 5px 0;">' . $field['label'] . '</div>';
			}
			elseif($field['type'] == 'number') {
				$gold_report .= '<b>'.$field['label'].":</b> ".$entry[$field['id']].'<br/>';
			}
			elseif($field['type'] == 'textarea') {
				$gold_report .= '<p><b>'.$field['label']."</b><br/>".$entry[$field['id']].'</p>';
			}
			elseif($field['type'] == 'number' && strpos($field['label'], 'Total') != false) {
				$gold_report .= '<b>'.$field['label']."</b> is ".$entry[$field['id']].'<br/>';
			}
			
		}
		
		$gold_report .= '</p>';
		
		return $gold_report;		
		
	}
	
}

if (!class_exists('BRRS_REVIEW')) {
	 return new BRRS_REVIEW;
 }