<?php

/**
 * ===================================================
 * Review Action: Assign
 * 
 * Assigning a review to a specified user with a supported
 * role.
 * 
 * Note: An author cannot be assigned a review.
 * 
 * Supported roles a user must have to be assigned a review
 * - auditor
 * - reviewer
 * - administrator 
 * 
 * ===================================================
 */
 
 class BRRS_REVIEW_ACTION_ASSIGN {
 	
	/**
	 * Assign a review to a user
	 * @param $review_id The review ID
	 * @param $current_user_id The current user's ID
	 * @param $new_assignee The user to assign the review to
	 * @return array
	 */
	static function assign_review( $review_id, $current_user_id, $new_assignee ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to assign review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		$review_author_id = get_post_field( 'post_author', $review_id );
		
		if( self::is_authorized( $current_user_id, $current_status ) ) {
		
			if( $review_author_id != $new_assignee ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_assignee != $new_assignee ) {
					
					// Update only if different
					if( $current_status != BRRS_REVIEW_STATUSES::assigned ) {
						$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::assigned );
					}
					else {
						$new_status_result = true;
					}
					if( $current_assignee != $new_assignee ) {
						$new_assignee_result = update_post_meta( $review_id, "brrs_reviewer_id", $new_assignee );
					}
					else {
						$new_assignee_result = true;
					}
						
					// Check if updates were succesful		
					if( $new_status_result && $new_assignee_result ) {
						
						$result['code'] = 1;
						$result['message'] = 'Review successfully assigned';
						self::send_emails($review_id);
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to fully assign review: [status=' . $new_status_result . '],[reviewer=' . $new_assignee_result .']';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully assigned';
					self::send_emails($review_id);
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review Cannot Be Assigned to the Author.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
			$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
			
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::assigned  
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Assignee - Advising they are assigned to perform the review
	 * - Author - General notice that their review has been assigned
	 * 
	 * @param $review_id
	 */
	static function send_emails( $review_id ) {
		
		// Send to Author of Manuscript
		self::author_email($review_id);
		// Send to Assigned Reviewer
		self::reviewer_email($review_id);
		
	}

	static function author_email( $review_id ) {
				
		$subject = "💡 [BRRS] Review Assignment (" . $review_id . ")";	
		$author = BRRS_REVIEW::get_author($review_id);
		if(is_object($author)){
			$to_email = $author->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-assigned-author.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
	static function reviewer_email( $review_id ) {
		
		$subject = "💡 [BRRS] Review Assignment (" . $review_id . ")";
		$reviewer = BRRS_REVIEW::get_reviewer($review_id);
		if(is_object($reviewer)){
			$to_email = $reviewer->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-assigned-reviewer.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
 }


 if (!class_exists('BRRS_REVIEW_ACTION_ASSIGN')) {
	 return new BRRS_REVIEW_ACTION_ASSIGN;
 }