<?php

/**
 * ===================================================
 * Review Action: Approve Review
 * 
 * Pulling a review from being reviewed. Places in "Pulled" status
 * Indicating to the reviewer to stop reviewing until reassigned.
 * 
 * Note: Must first be in a COMPLETE status AND assigned
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - reviewer
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_APPROVE {
 	
	/**
	 * Approve a review
	 * @param $review_id The review ID
	 * @param $current_user_id Current user's ID
	 * @return array
	 */
	static function approve_review( $review_id, $current_user_id ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to approve review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		
		if( self::is_authorized( $current_user_id ) ) {
		
			// Check if review is assigned and in the complete status before allowing it to be started.
			if( is_numeric( $current_assignee ) && $current_status == BRRS_REVIEW_STATUSES::complete ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_status != BRRS_REVIEW_STATUSES::approved ) {
					
					// Update only if different
					if( $current_status != BRRS_REVIEW_STATUSES::approved ) {
						$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::approved );
						$new_approved_date = update_post_meta( $review_id, "brrs_approved_date", date("m/d/Y") );
					}
					else {
						$new_status_result = true;
					}
						
					// Check if updates were successful		
					if( $new_status_result ) {
						
						$result['code'] = 1;
						$result['message'] = 'Review successfully approved.';
						self::send_emails($review_id);
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to approve review. [Status Update Failed]';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully approved.';
					self::send_emails($review_id);
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review must first be assigned AND in the COMPLETE status.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor', 'reviewer' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::approved  
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Assignee - Advising their review has been approved
	 * - Author - Final notice that their review has been approved
	 * 
	 * @param $review_id
	 * @param $user_id
	 * @return boolean
	 */
	static function send_emails( $review_id ) {
		
		// Send email to Author
		self::author_email($review_id);
		// Send email to Reviewer
		self::reviewer_email($review_id);
		
	}
	
	static function author_email($review_id) {
		
		$subject = "💡 [BRRS] Review Approved (" . $review_id . ")";	
		$author = BRRS_REVIEW::get_author($review_id);
		if(is_object($author)){
			$to_email = $author->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-approved-author.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
	static function reviewer_email( $review_id ) {
		
		$subject = "💡 [BRRS] Review Approved (" . $review_id . ")";
		$reviewer = BRRS_REVIEW::get_reviewer($review_id);
		if(is_object($reviewer)){
			$to_email = $reviewer->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-approved-reviewer.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
 }

 if (!class_exists('BRRS_REVIEW_ACTION_APPROVE')) {
 	return new BRRS_REVIEW_ACTION_APPROVE;
 }