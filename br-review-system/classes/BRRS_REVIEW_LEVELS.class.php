<?php

/**
 * ===================================================
 * Review Accreditation LEVEL enumerations 
 * ===================================================
 */
 
class BRRS_REVIEW_LEVELS {
	
    const level1 = 'Level 1';
	const level2 = 'Level 2';
	const level3 = 'Level 3';
	const shelveit = 'Shelve It';
	const pursueit = 'Pursue It';
	
	static public function get_levels() {
		
		$levels = array(
			self::level1,
			self::level2,
			self::level3,
			self::shelveit,
			self::pursueit
		);
		
		return $levels;
		
	}
	
}

if (!class_exists('BRRS_REVIEW_LEVELS')) {
	 return new BRRS_REVIEW_LEVELS;
 }