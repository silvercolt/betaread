<?php

class BRRS_SETTINGS {

	/**
	 * Holds the values to be used in the fields callbacks
	 */
	public $options;
	public $option_group = 'brrs';
	
	function __construct() {
		
		// Add Settings page
		add_action( 'admin_menu', array( $this, 'settings_submenu' ) );
		// Register Settings
		add_action( 'admin_init', array( $this, 'register_product_settings' ) );
		// Add Sections
		add_action( 'admin_init', array( $this, 'section_product_settings' ) );
		
	}
	  
	 /**
	 * Create Settings Sub-menu page
	 */	
	 function settings_submenu() {
	 	add_submenu_page( 
			'edit.php?post_type=review', //Parent Slug 
			__( 'Review Settings', 'BRRS' ), // Page Title
			__( 'Settings', 'BRRS' ), // Menu Title
			'manage_options', // Capability required
			'review_settings', // Menu Slug
			 array( $this, 'create_settings_page' ) // Content Call Back 
		);
	 }
	
	
	/**
	 * Create Settings Page (callback)
	 */
	 function create_settings_page() {
	 	
		// Set class property
        $this->options['review_product_packages'] = get_option( BR_OPTION_REVIEW_PACKAGES );
        ?>
        <div class="wrap">
            <h1><?php _e( 'Review Settings', 'BRRS' ); ?></h1>
            <?php settings_errors(); ?> 
            <div id="description">Different options required for the basic operation of the Beta|Review System (BRRS)</div>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( $this->option_group );
                do_settings_sections( 'brrs-settings-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
		
	 }
	
	/**
	 * Register Settings: Products
	 */
	 function register_product_settings() {
	 
		 // Review Packages Setting
	 	register_setting( 
	 		$this->option_group, 
	 		'review_product_packages', 
			array ( 
				'type' 				=> 'intval', 
				'description' 		=> 'Review Product Packages', 
				'sanitize_callback' => null, 
				'show_in_rest' 		=> false, 
				'default' 			=> null 
			) );

	 }
	 
	 /**
	  * Add Section: Product Settings
	  */
	 function section_product_settings() {
	 	
		// Product Section
		add_settings_section(
            'product_settings_section', // ID
            'Product Mapping', // Title
             array( $this, 'print_product_mapping_section_info' ), // Callback
            'brrs-settings-admin' // Page
        );  

        add_settings_field(
            'gold_package', // ID
            'Gold Package', // Title 
             array( $this, 'gold_package_callback' ), // Callback
            'brrs-settings-admin', // Page
            'product_settings_section', // Section
            array( 
            	'label_for' => 'gold_package',
            	'description' => 'The Gold Review Product'
            )          
        );      

        add_settings_field(
            'silver_package', // ID
            'Silver Package', // Title 
             array( $this, 'silver_package_callback' ), // Callback
            'brrs-settings-admin', // Page
            'product_settings_section', // Section
            array( 
            	'label_for' => 'silver_package',
            	'description' => 'The Silver Review Product' 
			)          
        );   
		
		add_settings_field(
            'bronze_package', // ID
            'Bronze Package', // Title 
             array( $this, 'bronze_package_callback' ), // Callback
            'brrs-settings-admin', // Page
            'product_settings_section', // Section 
            array( 
            	'label_for' => 'bronze_package',
            	'description' => 'The Bronze Review Product'
			)          
        );    
		
	 }

	/** 
     * Print the Product Mapping Section text
     */
    function print_product_mapping_section_info()
    {
        print 'Select the Products which correspond to the Review Packages:';
    }
	
	/** 
     * Gold Package Option
     */
    function gold_package_callback()
    {
    	$products = $this->get_product_listing();
		$gold_package_id = $this->options['review_product_packages']['gold_package'];
		print $this->product_select_builder( 'gold_package', $gold_package_id );
		
    }

	/** 
     * Silver Package Option
     */
    function silver_package_callback()
    {
		$silver_package_id = $this->options['review_product_packages']['silver_package'];
		print $this->product_select_builder( 'silver_package', $silver_package_id );
    }
	
	/** 
     * Bronze Package Option
     */
    function bronze_package_callback()
    {
    	$products = $this->get_product_listing();
		$bronze_package_id = $this->options['review_product_packages']['bronze_package'];
		print $this->product_select_builder( 'bronze_package', $bronze_package_id );
		
    }
	
	private function product_select_builder( $package_slug, $current_selection_id ) {
		
		$products = $this->get_product_listing();
		$html = '<select id="' . $package_slug . '" name="review_product_packages[' . $package_slug . ']">';
		// Set current selection, if available
		if( isset( $current_selection_id ) ) {
			$html .= '<option value="' . $current_selection_id .'" selected>'. get_the_title( $current_selection_id ) . '</option>';
		}
		else {
			$html .=  '<option value="none" selected>Choose a Product...</option>';
		}
		// Create list of products to select from
		foreach($products as $product) {
			$html .=  '<option value="' . $product['external_number'] .'">'. $product['product_name'] . '</option>';
		}
		$html .=  '</select>';
		
		return $html;
		
	}
	
	private function get_product_listing() {
		
		$full_product_list = array();
		$loop = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1));

	    while ($loop->have_posts()) : $loop->the_post();
	        $id = get_the_ID();
	        $product = new WC_Product($id);
	        
            $sku 			= get_post_meta($id, '_sku', true);
            $selling_price 	= get_post_meta($id, '_sale_price', true);
            $regular_price 	= get_post_meta($id, '_regular_price', true);
            $description	= get_the_content();
            $title 			= get_the_title();
			
	        if (!empty($sku))
	            $full_product_list[] = array(
	            	"description"		=> $description,
	                "external_number" 	=> $id,
	                "product_name" 		=> $title,
	                "sku" 				=> $sku,
                	"regular_price" 	=> $regular_price, 
	               	"selling_price" 	=> $selling_price
				);
	    endwhile; 
		
		return $full_product_list;
		
	}
 
}

if( is_admin() ) {
	if (!class_exists('BRRS_SETTINGS')) {
	 	return new BRRS_SETTINGS;
 	}
}
