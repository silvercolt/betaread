<?php

class BRRS_ABOUT {
	
	function __construct() {
		
		// Add Settings page
		add_action( 'admin_menu', array( $this, 'about_submenu' ) );
		// Add Sections
		add_action( 'admin_init', array( $this, 'about_sections' ) );
		
	}
	  
	 /**
	 * Create Settings Sub-menu page
	 */	
	 function about_submenu() {
	 	add_submenu_page( 
			'edit.php?post_type=review', //Parent Slug 
			__( 'About', 'BRRS' ), // Page Title
			__( 'About', 'BRRS' ), // Menu Title
			'manage_options', // Capability required
			'review_about', // Menu Slug
			 array( $this, 'create_about_page' ) // Content Call Back 
		);
	 }
	
	
	/**
	 * Create About Page (callback)
	 */
	 function create_about_page() {
		
		if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'general';
        }
        ?>
        <div class="wrap">
            <h1><?php _e( 'About', 'BRRS' ); ?></h1>
            <div class="description">A set of explanations and other documentation supporting the Beta|Read Review System (BRRS)</div>
            
            <h2 class="nav-tab-wrapper">  
            	<a href="?post_type=review&page=review_about&tab=general" class="nav-tab <?php echo $active_tab == 'general' ? 'nav-tab-active' : ''; ?>">General</a>  
            	<a href="?post_type=review&page=review_about&tab=glossary" class="nav-tab <?php echo $active_tab == 'glossary' ? 'nav-tab-active' : ''; ?>">Glossary</a>  
        	</h2> 
            
            <?php
                if( $active_tab == 'general' ) {  

                    do_settings_sections( 'brrs-about-general' );

                } else if( $active_tab == 'glossary' )  {

                    do_settings_sections( 'brrs-about-glossary' );

				} 
            ?>
        </div>
        <?php
	 }
	 
	 /**
	  * About Sections
	  */
	 function about_sections() {
	 	
		add_settings_section(
            'section_about_general', // ID
            'General', // Title
             array( $this, 'print_about_general_section_info' ), // Callback
            'brrs-about-general' // Page
        );  
		
		add_settings_section(
            'section_about_glossary', // ID
            'Glossary', // Title
             array( $this, 'print_about_glossary_section_info' ), // Callback
            'brrs-about-glossary' // Page
        );  
		
	 }

	/** 
     * Print the General About Section text
     */
    function print_about_general_section_info()
    {
        //TODO: Add General Information About the BRRS
    }

	/** 
     * Print the Glossary About Section text
     */
    function print_about_glossary_section_info()
    {
        //TODO: Add Glossary of Terms used throughout the BRRS
    }
	
 
}

if( is_admin() ) {
	 if (!class_exists('BRRS_ABOUT')) {
	 	return new BRRS_ABOUT;
	 }
}
