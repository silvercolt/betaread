<?php

/**
 * ===================================================
 * Review Action: Complete Review
 * 
 * Completing the review process of a review.  
 * 
 * Note: Must first be assigned to a reviewer, auditor, or administrator
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - reviewer
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_COMPLETE {
 	
	/**
	 * Complete a review
	 * @param $review_id The review ID
	 * @param $current_user_id Current user's ID
	 * @param $accreditation The selected Accreditation Level
	 * @param $comment a final comment as to why
	 * @return array
	 */
	static function complete_review( $review_id, $current_user_id, $accreditation, $comment ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to complete review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		$current_accreditation = get_post_meta( $review_id, "brrs_accreditation", true );
		
		if( self::is_authorized( $current_user_id, $current_status ) ) {
		
			// Check if review is assigned before allowing it to be started.
			if( is_numeric( $current_assignee ) ) {
				
				// Check if the current values against the new ones, 
				// if they match then just return TRUE - no changes necessary
				if( $current_status != BRRS_REVIEW_STATUSES::complete || $current_accreditation != $accreditation ) {
					
					if( $current_status != BRRS_REVIEW_STATUSES::complete ) {
						$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::complete );
					}
					else {
						$new_status_result = true;
					}
					
					if( $current_accreditation != $accreditation ) {
						$new_accreditation_level = update_post_meta( $review_id, "brrs_accreditation", $accreditation );
					}
					else {
						$new_accreditation_level = true;
					}
					
					$user_data = get_userdata($current_user_id);
					$comment_data = array(
						'comment_post_ID' => $review_id,
						'comment_author' => $user_data->display_name,
						'comment_author_email' => $user_data->user_email, 
						'comment_author_url' => null, 
						'comment_content' => $comment,
						'comment_type' => '',
						'comment_parent' => 0,
						'user_id' => $current_user_id,
					);
					$new_comment_result = wp_new_comment( $comment_data, true );
						
					// Check if updates were successful		
					if( $new_status_result ) {
						
						if(  !is_numeric($new_comment_result) ) {
							$result['code'] = 1;
							$result['message'] = 'Review successfully completed. [However, the comment failed to save].';
							self::send_emails($review_id);
						}
						
						else if( !$new_accreditation_level ) {
							$result['code'] = 1;
							$result['message'] = 'Review successfully completed. [However, the accreditation failed to save].';
							self::send_emails($review_id);
						}	
						
						else {
							$result['code'] = 1;
							$result['message'] = 'Review successfully completed.';
							self::send_emails($review_id);
						}	
						
					}
					else {
						
						$result['code'] = 0;
						$result['message'] = 'Failed to complete review. [Status Update Failed]';
						
					}
					
				}
				else {
					
					$result['code'] = 1;
					$result['message'] = 'Review successfully completed.';
					self::send_emails($review_id);
					
				}
				
			}
			else {
				
				$result['code'] = 0;
				$result['message'] = 'Review must first be assigned.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'auditor', 'reviewer' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::complete 
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Author - General notice that their review has been completed
	 * - Auditor - Review has been completed, ready for your approval
	 * 
	 * @param $review_id
	 * @return boolean
	 */
	static function send_emails( $review_id ) {
		
		// Send email to Author
		self::author_email($review_id);
		// Send email to Auditors
		self::auditor_email($review_id);
		
	}
	
	static function author_email($review_id) {
		
		$subject = "💡 [BRRS] Review Completed (" . $review_id . ")";	
		$author = BRRS_REVIEW::get_author($review_id);
		if(is_object($author)){
			$to_email = $author->user_email;
		}

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-completed-author.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, $to_email, null, $template);
		
	}
	
	static function auditor_email($review_id) {
		
		$subject = "💡 [BRRS] Review Ready For Approval (" . $review_id . ")";	
		$bcc_emails = array();
		$users = get_users( array( 'role__in' => array ( 'auditor', 'administrator' ) ) );
		foreach($users as $user) {
			$bcc_emails[] = $user->user_email;
		}	

		$template = BRRS_PLUGIN_PATH . "templates/emails/brrs-email-completed-auditor.template.php";

		BRRS_REVIEW::send_emails($review_id, $subject, null, $bcc_emails, $template);
		
	}
	
 }

 if (!class_exists('BRRS_REVIEW_ACTION_COMPLETE')) {
	 return new BRRS_REVIEW_ACTION_COMPLETE;
 }