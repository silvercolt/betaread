<?php

/**
 * ===================================================
 * Review STATUS enumerations 
 * ===================================================
 */
 
class BRRS_REVIEW_STATUSES {
	
    const queued = 'Queued';
	const approved = 'Approved';
	const assigned = 'Assigned';
	const inProgress = 'In Progress';
	const complete = 'Complete';
	const closed = 'Closed';
	const pulled = 'Pulled';
	const accepted = 'Accepted';
	
	static public function get_statuses() {
		
		$statuses = array(
			self::queued,
			self::assigned,
			self::approved,
			self::inProgress,
			self::complete,
			self::closed,
			self::pulled,
			self::accepted
		);
		
		return $statuses;
		
	}
	
}

if (!class_exists('BRRS_REVIEW_STATUSES')) {
	 return new BRRS_REVIEW_STATUSES;
 }