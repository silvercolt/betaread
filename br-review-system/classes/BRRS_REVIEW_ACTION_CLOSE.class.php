<?php

/**
 * ===================================================
 * Review Action: Close Review
 * 
 * Closing a review process of a review.  
 * 
 * Supported roles a user must have to start a review
 * - auditor
 * - administrator 
 * 
 * ===================================================
 */
 
class BRRS_REVIEW_ACTION_CLOSE {
 	
	/**
	 * Close a review
	 * @param $review_id The review ID
	 * @param $current_user_id Current user's ID
	 * @param $comment a final comment as to why
	 * @return array
	 */
	static function close_review( $review_id, $current_user_id, $comment ) {
		
		$result = array(
			"code" 		=> 0,
			"message" 	=> "Failed to close review"
		);
		
		$current_status = get_post_meta( $review_id, "brrs_status", true );
		$current_assignee = get_post_meta( $review_id, "brrs_reviewer_id", true );
		
		if( self::is_authorized( $current_user_id ) ) {
				
			// Check if the current values against the new ones, 
			// if they match then just return TRUE - no changes necessary
			if( $current_status != BRRS_REVIEW_STATUSES::closed ) {
				
				$new_status_result = update_post_meta( $review_id, "brrs_status", BRRS_REVIEW_STATUSES::closed );
				$user_data = get_userdata($current_user_id);
				$comment_data = array(
					'comment_post_ID' => $review_id,
					'comment_author' => $user_data->display_name,
					'comment_author_email' => $user_data->user_email, 
					'comment_author_url' => null, 
					'comment_content' => $comment,
					'comment_type' => '',
					'comment_parent' => 0,
					'user_id' => $current_user_id,
				);
				$new_comment_result = wp_new_comment( $comment_data, true );

				// Check if updates were successful		
				if( $new_status_result) {
					
					if(  is_numeric($new_comment_result) ) {
						$result['code'] = 1;
						$result['message'] = 'Review successfully closed.';
					}
					else {
						$result['code'] = 1;
						$result['message'] = 'Review successfully closed. [However, the comment failed to save].';
					}					
					
				}
				else {
					
					$result['code'] = 0;
					$result['message'] = 'Failed to close review. [Status Update Failed]';
					
				}
				
			}
			else {
				
				$result['code'] = 1;
				$result['message'] = 'Review successfully closed.';
				
			}
		
		} // Authorization Check
		else {
			
			$result['code'] = 0;
				$result['message'] = 'You are not authorized to perform this action';
			
		}
		
		return $result;
		
	}
	
	/**
	 * Does the specified user have permission to this action?
	 * Is the review's current status allow this action?
	 * @param $current_user_id Current user's ID
	 * @param $current_status Current review's status
	 * @return boolean
	 */
	static function is_authorized( $current_user_id, $current_status ) {
		
		$user = get_userdata( $current_user_id );
		$auth_user_roles = array( 'administrator', 'reviewer' );
		$user_roles = $user->roles;
		
		// Authorization Check		
		if ( 
			0 !== $current_user_id && 
			!empty( array_intersect( $user_roles, $auth_user_roles ) ) &&
			$current_status != BRRS_REVIEW_STATUSES::closed  
		) {
			
			return true;
			
		}
		else {
			
			return false;
			
		}
		
	}
	
	/**
	 * Send notifications about this action
	 * Recipients:
	 * - Assignee - Advising they are assigned to perform the review
	 * - Author - General notice that their review has been assigned
	 * 
	 * @param $review_id
	 * @param $user_id
	 * @return boolean
	 */
	static function send_emails( $review_id, $user_id ) {
		
		
		
	}
	
 }


 if (!class_exists('BRRS_REVIEW_ACTION_CLOSE')) {
	 return new BRRS_REVIEW_ACTION_CLOSE;
 }